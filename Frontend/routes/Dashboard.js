if (Meteor.isClient) {

  Meteor.startup(function() {

  	Router.route('/', function() {
  		
  		this.render('Header', {to: 'header'});
  		this.render('Sidebar', {to: "sidebar"});
  		this.render('Dashboard', {to: "content"});
  	});
  });
}