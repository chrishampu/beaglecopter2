Data = new Mongo.Collection("BeagleData");

if (Meteor.isClient) {

	Meteor.startup(function() {

		// Track connection status to beaglecopter on client
		Tracker.autorun(function () {
			Meteor.subscribe('connection-status');

			var connection = Data.findOne("connectionStatus");

			if(connection !== undefined)
			{
				if(connection.connected === true)
				{
					$($$("connection-status").getNode()).html("connected");
					$(".connection-status").addClass("connection-status-connected").removeClass("connection-status-disconnected");
				}
				else
				{
					$($$("connection-status").getNode()).html("disconnected");
					$(".connection-status").addClass("connection-status-disconnected").removeClass("connection-status-connected");
				}
			}
		});

		Tracker.autorun(function () {
			Meteor.subscribe('sensor-data');

			var data = Data.findOne("sensorData");

			if(data !== undefined)
			{
				$$("sensor-data").setValues({
					yaw: data.yaw,
					pitch: data.pitch,
					roll: data.roll,
					temperature: data.temp,
					altitude: data.altitude
				});
			}
		});

		Tracker.autorun(function () {
			Meteor.subscribe('throttle-data');

			var data = Data.findOne("throttleData");

			if(data !== undefined)
			{
				if(data.motorN === undefined)
					return;
				$$("throttleMotorN").setValue("Motor 1: " + data.motorN / 10 + "%");
				$$("throttleMotorE").setValue("Motor 2: " + data.motorE / 10 + "%");
				$$("throttleMotorS").setValue("Motor 3: " + data.motorS / 10 + "%");
				$$("throttleMotorW").setValue("Motor 4: " + data.motorW / 10 + "%");
			}
		});

		Tracker.autorun(function () {
			Meteor.subscribe('pid-data');

			var data = Data.findOne("pidData");

			if(data !== undefined)
			{
				if(data.yaw === undefined)
					return;
				$$("pid-data").setValues({
					"yaw-pid": data.yaw.toPrecision(2),
					"pitch-pid": data.pitch.toPrecision(2),
					"roll-pid": data.roll.toPrecision(2),
				});
			}
		});

		Tracker.autorun(function () {
			Meteor.subscribe('pid-tuning-data');

			var data = Data.findOne("pidTuneData");

			if(data !== undefined)
			{
				if(data.pterm === undefined)
					return;
				$$("PPIDSlider").setValue(data.pterm * 100);
				$$("IPIDSlider").setValue(data.iterm * 100);
				$$("DPIDSlider").setValue(data.dterm * 100);
			}
		});
	});
}

if (Meteor.isServer) {

	var http = Npm.require('http');
	var wsocket;
	var connection = undefined;

	// Auto tracking of copter connection status
	Meteor.publish('connection-status', function sendConnectionStatus() {

		var self = this;
		Data.upsert("connectionStatus", {$set: {connected: false}});

		var handle = Data.find("connectionStatus").observeChanges({
			changed: function (id, fields) {

				self.changed("BeagleData", "connectionStatus", {connected: fields.connected});
			}
		});

		self.added("BeagleData", "connectionStatus", {connected: false});
		self.ready();

		self.onStop(function() {
			handle.stop();
		});
	});

	Meteor.publish('sensor-data', function sendSensorData(data) {
		var self = this;

		Data.upsert("sensorData", {});

		var handle = Data.find("sensorData").observeChanges({
			changed: function (id, fields) {

				self.changed("BeagleData", "sensorData", fields);
			}
		});

		self.added("BeagleData", "sensorData", {});
		self.ready();

		self.onStop(function() {
			handle.stop();
		});
	});

	Meteor.publish('throttle-data', function sendThrottleData(data) {
		var self = this;
		var cachedThrottle = 0;

		Data.upsert("throttleData", {});

		var handle = Data.find("throttleData").observeChanges({
			changed: function (id, fields) {
				self.changed("BeagleData", "throttleData", fields);

				if(fields.throttle !== undefined && cachedThrottle != fields.throttle)
				{
					cachedThrottle = fields.throttle;
					if(connection !== undefined)
					{
						var out = "{ \"throttle\": " + cachedThrottle + " }";
						connection.write(out);
					}
				}
			}
		});

		self.added("BeagleData", "throttleData", {});
		self.ready();

		self.onStop(function() {
			handle.stop();
		});
	});

	Meteor.publish('pid-data', function sendPIDData(data) {
		var self = this;

		Data.upsert("pidData", {});

		var handle = Data.find("pidData").observeChanges({
			changed: function (id, fields) {
				self.changed("BeagleData", "pidData", fields);
			}
		});

		self.added("BeagleData", "pidData", {});
		self.ready();

		self.onStop(function() {
			handle.stop();
		});
	});

	Meteor.publish('pid-tuning-data', function sendPIDTuneData(data) {
		var self = this;
		var firstRun = true;
		var cachedPTerm = -1, cachedITerm = -1, cachedDTerm = -1;

		Data.upsert("pidTuneData", {});

		var handle = Data.find("pidTuneData").observeChanges({
			changed: function (id, fields) {
				self.changed("BeagleData", "pidTuneData", fields);

				if(fields.pterm !== undefined && cachedPTerm != fields.pterm)
					cachedPTerm = fields.pterm;
				if(fields.iterm !== undefined && cachedITerm != fields.iterm)
					cachedITerm = fields.iterm;
				if(fields.dterm !== undefined && cachedDTerm != fields.dterm)
					cachedDTerm = fields.dterm;

				// The first update is the copter sending us the initial values
				if(firstRun) {
					firstRun = false;
					return;
				}

				if(connection !== undefined)
				{
					var out = "{ \"pidtuning\": { \"pterm\": " + cachedPTerm + ", \"iterm\": " + cachedITerm + ", \"dterm\": " + cachedDTerm + " }}";
					connection.write(out);
				}
			}
		});

		self.added("BeagleData", "pidTuneData", {});
		self.ready();

		self.onStop(function() {
			firstRun = false;
			handle.stop();
		});
	});

	Meteor.startup(function () {
		wsocket = sockjs.createServer({ sockjs_url: 'http://cdn.jsdelivr.net/sockjs/0.3.8/sockjs.min.js',
			log: function(severity, message) { console.log(message); } });

		// Send/receive data as well as update connection status
		wsocket.on('connection', Meteor.bindEnvironment( function(conn) {

			Data.update("connectionStatus", {$set: {connected: true}});
			connection = conn;

			conn.on('data', Meteor.bindEnvironment( function(message) {
				var data = JSON.parse(message);

				if(data.sensors !== undefined)
				{
					Data.update("sensorData", data.sensors);
				}
				if(data.throttles !== undefined)
				{
					Data.update("throttleData", data.throttles);
				}
				if(data.pid !== undefined)
				{
					Data.update("pidData", data.pid);
				}
				if(data.pidtuning !== undefined)
				{
					Data.update("pidTuneData", data.pidtuning);
				}
			}));

			conn.on('close', Meteor.bindEnvironment( function() {
				Data.update("connectionStatus", {$set: {connected: false}});
				connection = undefined;
			}));

		}, function(err) { console.log(err)}));

		var server = http.createServer();
		wsocket.installHandlers(server, {prefix:'/socket'});
		server.listen(9999, '0.0.0.0');
	});
}
