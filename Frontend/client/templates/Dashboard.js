if (Meteor.isClient) {

    Template.Dashboard.rendered = function() {

        webix.ui({
			rows: [
				{ view: "label", height: 49, id: "content-title", css: "content-title", template: "Dashboard", data: {title:"", details: ""} },
				{
					view: "scrollview", scroll:"native-y",
					body:{ 
						type: "clean",
						rows: [{
							type: "space",
							rows: [{
								type: "material", height: 250,
								cols: [
									{
										rows: [{
												view: "template",
												template: "Motor Control",
												type: "header",
												css: "dashboard-header header-orange"
											},
											{
												view: "label",
												id: "motor-status",
												label: "<b>Status:</b> Motors not running",
												align: "center"
											},
											{
												type: "line",
												cols: [{
													rows: [
														{
															view: "form",
															width: "50%",
															height: "100%",
															elements: [
																{
																	view: "button",
																	id: "togger-motor-button",
																	css: "button_success button_raised",
																	label: "Toggle motors",
																	width: 175,
																	margin: 5,
																	align: "center"
																},
																{
																	view: "slider", 
																	label: "Throttle", 
																	value: "0", 
																	min: 0, max: 100, 
																	name: "Throttle",
																	paddingY: 15,
																	title: webix.template("#value#%"),
																	id: "ThrottleSlider",
																	css: "throttle-slider"
																}
															]
														}
													]

												},
												{
													type: "line",
													rows: [
														{
															view: "label", id: "throttleMotorN", label: "Motor 1: 0%", align: "center", height: 55
														},
														{
															type: "line",
															cols: [
																{ view: "label", id: "throttleMotorW", label: "Motor 4: 0%",align: "center",height: 55 },
																{ view: "label", id: "throttleMotorE", label: "Motor 2: 0%",align: "center" }
															]
														},
														{
															view: "label", id: "throttleMotorS",label: "Motor 3: 0%",align: "center", height: 55
														}

													]
												}]
										}]

									},
									{
										view: "property", id: "sensor-data", css: "sensor-properties",
							  			elements: [
									  		{ label: "Sensor Data", type: "label"},
									  		{ label: "Yaw", type: "text", id: "yaw", value: 0.0},
									  		{ label: "Pitch", type: "text", id: "pitch", value: 0.0},
									  		{ label: "Roll", type: "text", id: "roll", value: 0.0},
									  		{ label: "Temperature", type: "text", id: "temperature", value: 0.0},
									  		{ label: "Altitude", type: "text", id: "altitude", value: 0}
									  	]
									}
								]},
								{
								type: "material", height: 250,
								cols: [
									{
										rows: [
											{
												view: "template",
												template: "PID Tuning",
												type: "header",
												css: "dashboard-header header-purple"
											},
											{
												css: "pid-tuning",
												cols: [
													{
														view: "slider",
														label: "P Term",
														value: "0", 
														min: 0,
														max: 300, 
														step: 1,
														title: function(obj) {
															return "" + obj.value / 100;
														},
														id: "PPIDSlider",
														inputWidth: 400
													},
													{
														view: "slider", 
														label: "I Term",
														value: "0", 
														min: 0, 
														max: 300,
														step: 1,
														title: function(obj) {
															return "" + obj.value / 100;
														},
														id: "IPIDSlider",
														inputWidth: 400
													},
													{
														view: "slider", 
														label: "D Term",
														value: "0", 
														min: 0, 
														max: 300,
														step: 1,
														title: function(obj) {
															return "" + obj.value / 100;
														},
														id: "DPIDSlider",
														inputWidth: 400
													}
												]
											},
											{
												view: "property", css: "sensor-properties", id: "pid-data",
									  			elements: [
											  		{ label: "Yaw", type: "text", id: "yaw-pid", value: 0.0},
											  		{ label: "Pitch", type: "text", id: "pitch-pid", value: 0.0},
											  		{ label: "Roll", type: "text", id: "roll-pid", value: 0.0}
											  	]
											}
										]
									}
								]
							}]
						}]
					}
				}
			]
        }, $$('body-container'), $$('content-container'));

		$$("ThrottleSlider").attachEvent("onSliderDrag", function() {
			Data.update("throttleData", { $set: { throttle: this.getValue() }});
		});

		// In order to not overwhelm the AutoPilot with recalculating its tuning parameters,
		// we ensure a slight dely before committing the slider values to the AutoPilot.
		var SliderTimeout;

		$$("PPIDSlider").attachEvent("onSliderDrag", function() {

			clearTimeout(SliderTimeout);
			var that = this;

			SliderTimeout = setTimeout(function() {
				Data.update("pidTuneData", { $set: { pterm: that.getValue() / 100}});
			}, 100);
		});

		$$("IPIDSlider").attachEvent("onSliderDrag", function() {
			clearTimeout(SliderTimeout);
			var that = this;

			SliderTimeout = setTimeout(function() {
				Data.update("pidTuneData", { $set: { iterm: that.getValue() / 100}});
			}, 100);
		});

		$$("DPIDSlider").attachEvent("onSliderDrag", function() {
			clearTimeout(SliderTimeout);
			var that = this;

			SliderTimeout = setTimeout(function() {
				Data.update("pidTuneData", { $set: { dterm: that.getValue() / 100}});
			}, 100);
		});

    };
};