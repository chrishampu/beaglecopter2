if (Meteor.isClient) {
    webix.type(webix.ui.tree, {
        name:"menuTree",
        height: 40,

        icon:function(obj, common){
            var html = "";
            var open = "";
            for (var i=1; i<=obj.$level; i++){
                if (i==obj.$level && obj.$count){
                    var dir = obj.open?"down":"right";
                    html+="<span class='"+open+" webix_icon fa-angle-"+dir+"'></span>";
                }
            }
            return html;
        },
        folder:function(obj, common){
            if(obj.icon)
                return "<span class='webix_icon icon fa-"+obj.icon+"'></span>";
            return "";
        }
    });

    Template.Sidebar.rendered = function() {

        var menu = {
            width: 200,
            rows: [{
                view: "tree",
                id: "sidebar-menu",
                type: "menuTree",
                css: "menu",
                activeTitle: true,
                select: true,
                data: [
                    {id: "dashboard", value: "Dashboard", icon: "home", css: "dashboard", details: "Main dashboard"},
                    {id: "other", value: "Other", icon: "search", css: "search", details: "Search"}
                ]
            }]
        };

        webix.ui({

            rows: [
                menu
            ]
        }, $$('body-container'), $$('sidebar-container'));
    };
};