if (Meteor.isClient) {

	Meteor.startup(function() {

		var container = {
		    cols: [
			  { id: "sidebar-container" },
			  { id: "content-container" }
			]
		}

	    webix.ui({
	    	margin: 0,
	    	id: "application-layout",
	    	rows: [
	    		{ id: "header-container" },
	    		{
	    			
	    			id: "body-container",
	    			cols: [
					  { id: "sidebar-container" },
					  { id: "content-container" }
					]
	    		}
	    	]
	    });
	});
};