if (Meteor.isClient) {
    Template.Header.rendered = function() {
        var header = {
            type:"material",
            view:"toolbar",
            css:"header",
            height: 50,
            elements:[
            {view: "template", borderless: true, css: "logo", template: "<a href='#'>BeagleCopter Control Panel</a>", width: 250},
            {},
            {view: "label", id: "connection-status", css: "connection-status connection-status-disconnected", template: "Disconnected", width: 150},
            {},
            {view: "icon", icon: "search",  width: 45}]
        };

        webix.ui({
            rows: [
                header
            ]
        }, $$('application-layout'), $$('header-container'));
    }
};