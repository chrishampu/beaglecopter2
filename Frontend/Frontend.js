if (Meteor.isClient) {

  Meteor.startup(function() {

    Router.configure({
      layoutTemplate: 'ApplicationLayout'
    });
  });
}

if (Meteor.isServer) {
  Meteor.startup(function () {

  });
}
