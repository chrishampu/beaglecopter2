#pragma once

#include "NetHTTPBuffer.h"

#include <string>
#include <sstream>
#include <algorithm>

// Base request template
template<class T>
class HTTPRequest
{
public:
	static const T Method;
	static const T Url;
};

// Request buffer class
class HTTPRequestBuffer 
	: public HTTPBuffer < HTTPRequest <std::string> >
{
private:
	// Self type, allows for method chaining
	typedef HTTPBuffer < HTTPRequest <std::string> >  _MyBuffer;
	typedef HTTPRequestBuffer _MyT;

	typedef std::string http_string_type;

	// Class parameters
	http_string_type _Method;
	http_string_type _Url;

public:
	// Populate class parameters from an existing string buffer of HTTP data

	HTTPRequestBuffer()
	{

	}

	explicit HTTPRequestBuffer(http_string_type buffer)
	{
		ConsumeBuffer(buffer);
	}

	// Copy constructor
	HTTPRequestBuffer(const _MyT& right)
		: _MyBuffer(right)
	{
		_Method = right._Method;
		_Url = right._Url;
	}

	// Move constructor
	HTTPRequestBuffer(_MyT&& right)
		: _MyBuffer(std::move(right))
	{
		std::swap(_Method, right._Method);
		std::swap(_Url, right._Url);
	}

	// Copy operator
	HTTPRequestBuffer& operator=(_MyT &right)
	{
		_MyBuffer::operator=(right);
		_Method = right._Method;
		_Url = right._Url;
		return (*this);
	}

	// Move operator
	HTTPRequestBuffer& operator=(_MyT&& right)
	{
		_MyBuffer::operator=(std::move(right));
		std::swap(_Method, right._Method);
		std::swap(_Url, right._Url);
		return (*this);
	}

	_MyT& SetMethod(http_string_type method)
	{
		_Method = method;

		return (*this);
	}

	_MyT& SetUrl(http_string_type url)
	{
		_Url = url;

		return (*this);
	}

	inline http_string_type Method()
	{
		return _Method;
	}

	inline http_string_type Url()
	{
		return _Url;
	}

	_MyT& ConsumeBuffer(http_string_type buffer)
	{
		http_string_iterator begin = buffer.begin();
		http_string_iterator end;

		for (;;)
		{
			// Grab the next new line
			end = SearchHeader(begin, buffer.end(), "\r\n");

			// No more data to read
			if (end == buffer.end())
				return (*this);

			// We found a blank line in this case, and the request buffer is complete
			if (end - begin == 0)
			{
				return (*this);
			}
			else
			{
				if (_Method.length() == 0)
				{
					// Process method
					http_string_iterator cursor_start = begin;
					http_string_iterator cursor_end = std::find(begin, end, ' ');

					if (cursor_end == end) {
						continue;
					}

					SetMethod(http_string_type(cursor_start, cursor_end));

					cursor_start = cursor_end + 1;
					cursor_end = std::find(cursor_start, end, ' ');

					if (cursor_end == end) {
						continue;
					}

					SetUrl(http_string_type(cursor_start, cursor_end));
					//std::string version = std::string(cursor_end + 1, end);
				}
				else
				{
					// Process header
					http_string_iterator cursor = SearchHeader(begin, end, ":");

					// Invalid header
					if (cursor == end) {
						continue;
					}

					SetHeader(http_string_type(begin, cursor), http_string_type(cursor + sizeof(":"), end));
				}
			}

			// Move to next new line, and skip delimiters
			begin = end + (sizeof("\r\n") - 1);
		}
	}
};