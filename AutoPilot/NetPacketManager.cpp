#include "NetPacketManager.h"

#include <stdio.h>
#include <map>
#include <memory>
#include <iostream>

namespace
{
	std::map<unsigned short, PacketHandle> PacketList;
	typedef std::map<unsigned short, PacketHandle>::iterator PacketListIter;
}

/**
	Macro for defining new packets so they can be processed when received.
	*/
#define IMPLEMENT(x) \
void Handle_Packet ##x (NetStream& b, NetSession* sess); \
static NetPacketManager Control_Packet ##x(pkt ##x, Handle_Packet ##x); \
void Handle_Packet ##x (NetStream& b, NetSession* sess) \
{ \
	Packet ##x *p; \
	p = new Packet ##x (); \
	p->Handle(b, sess); \
	if(p != NULL ) { \
		delete p; \
		p = NULL; \
	} \
}

/**
	And here we implement all of our packets.
*/
IMPLEMENT(JSONPayload)

NetPacketManager::NetPacketManager()
{

}

NetPacketManager::~NetPacketManager()
{
	PacketList.clear();
}

NetPacketManager::NetPacketManager(unsigned short id, PacketHandle handle)
{
	Register(id, handle);
}

void NetPacketManager::processPacketReceivedEvent(NetPacket *packet, NetSession *session)
{
	static union {
		unsigned short id;
		unsigned char c[2];
	} packetData;

	PacketListIter itr;

	packet->PacketData.peek(packetData.c, 2); // Now we can peek into our stream to grab our packet ID

	itr = PacketList.find(packetData.id); // Attempt to find a handle that matches our packet ID

	if (itr == PacketList.end()) {// Attempt to interpret packet as an HTTP packet
		//Console::Instance()->printf(Networking, "NetPacketManager::processPacketReceivedEvent(): Unknown packet %d received from %s. Dumping packet.\n", packetData.id, session->getAddressString().c_str());
		std::cout << "Unknown packet received" << std::endl;
		return;
	}

	PacketHandle handle = itr->second; // Grab our handle

	(*handle)(packet->PacketData, session); // Run our handle callback!
}

void NetPacketManager::Register(unsigned short id, PacketHandle handle)
{
	PacketList[id] = handle;
}