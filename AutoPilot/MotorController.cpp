#include "MotorController.h"
#include "NetWebsocketParser.h"
#include <thread>
#include <chrono>
#include <iostream>

static int MotorUpdateFrequency = 66; // 66ms = ~15 updates per second

MotorController::MotorController()
{
	Motors[North] = new Motor("/sys/devices/ocp.3/pwm_test_P9_31.10", North);
	Motors[East]  = new Motor("/sys/devices/ocp.3/pwm_test_P9_16.11", East);
	Motors[South] = new Motor("/sys/devices/ocp.3/pwm_test_P9_29.13", South);
	Motors[West]  = new Motor("/sys/devices/ocp.3/pwm_test_P9_14.12", West);
}

MotorController::~MotorController()
{
	for(auto& motor : Motors)
	{
		delete motor.second;
	}

	Motors.clear();
}

void MotorController::Update()
{
	auto Ticks = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::duration<double, std::milli>(MotorUpdateFrequency)).count();

	auto Now = std::chrono::steady_clock::now();
	auto CurrentCount = std::chrono::duration_cast<std::chrono::microseconds>(Now - LastUpdate);

	if(CurrentCount.count() > Ticks)
	{
		NetWebsocketParser::Instance()->SendMotorThrottles(Motors[North]->GetThrottle(), 
			Motors[East]->GetThrottle(), Motors[South]->GetThrottle(), Motors[West]->GetThrottle());

		LastUpdate = Now;
	}
}

void MotorController::ArmMotors()
{
	std::cout << "Arming motors. Delaying for 3 seconds" << std::endl;

	for(auto& motor : Motors)
	{
		motor.second->Arm();
	}

	// Have to wait for all the beeps to finish before we continue
	std::this_thread::sleep_for(std::chrono::seconds(3));

	std::cout << "Motors armed" << std::endl;
}

void MotorController::StopMotors()
{
	for(auto& motor : Motors)
	{
		motor.second->Stop();
	}
}

void MotorController::StartMotors()
{
	for(auto& motor : Motors)
	{
		motor.second->Run();
	}
}