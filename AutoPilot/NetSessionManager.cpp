#include "NetSessionManager.h"

NetSessionManager::NetSessionManager()
{

}

NetSessionManager::~NetSessionManager()
{
	ClearSessions();
}

void NetSessionManager::ClearSessions()
{
	for (SessionMap::iterator itr = Sessions.begin(), next; itr != Sessions.end(); itr = next)
	{
		next = itr;
		++next;

		delete itr->second;
	}

	Sessions.clear();
}

void NetSessionManager::UpdateSessions()
{
	for (SessionMap::iterator itr = Sessions.begin(), next; itr != Sessions.end(); itr = next)
	{
		next = itr;
		++next;

		NetSession* pSession = itr->second;

		if(pSession->ConnectionState == Connected)
		{
			if (!pSession->Update())
			{
				Sessions.erase(itr);
				delete pSession;
			}
		}
	}
}

NetSession* NetSessionManager::FindSession(NetSocket sock)
{
	SessionMap::const_iterator itr = Sessions.find(sock);

	if (itr != Sessions.end())
		return itr->second;
	else
		return NULL;
}

NetSession* NetSessionManager::FindSession(NetAddress addr)
{
	for (SessionMap::const_iterator itr = Sessions.begin(); itr != Sessions.end(); itr++)
		if (addr == itr->second->getAddress())
			return itr->second;

	return NULL;
}

void NetSessionManager::AddSession(NetSession *session)
{
	Sessions[session->getSocket()] = session;
}

void NetSessionManager::RemoveSession(NetSession *session)
{
	SessionMap::const_iterator itr = Sessions.find(session->getSocket());

	if (itr != Sessions.end() && itr->second)
	{
		//Console::Instance()->printf(Networking, "NetSessionManager::RemoveSessio(): Removing session %d\n", session->getSocket());
		delete itr->second;
		Sessions.erase(itr);
	}
}