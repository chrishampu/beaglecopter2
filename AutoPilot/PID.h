#pragma once

#include <atomic>

const float DefaultDeltaTime = 8.f; // Update rate in milliseconds. Default: 8ms (125hz)

extern std::atomic<float> TuningPTerm;
extern std::atomic<float> TuningITerm;
extern std::atomic<float> TuningDTerm;

class PID
{
public:
	// Tuning values
	PID(float tP, float tI, float tD);
	~PID();

	void TuneParameters(float FixedDeltaTime);
	void TuneParameters(float tP, float tI, float tD);
	void TuneParameters(float FixedDeltaTime, float tP, float tI, float tD);

	float Calculate(float Target, float Current);

	const float GetPTerm() const { return PTerm; };
	const float GetITerm() const { return ITerm; };
	const float GetDTerm() const { return DTerm; };
	float GetDeltaTime() { return DeltaTime; }

	const float GetTunePTerm() const { return TunePTerm; };
	const float GetTuneITerm() const { return TuneITerm; };
	const float GetTuneDTerm() const { return TuneDTerm; };

private:
	// Actual terms
	float PTerm, ITerm, DTerm;

	// Tuning gains
	float TunePTerm, TuneITerm, TuneDTerm;

	// Tuning gains adjusted by time
	float TimedPTerm, TimedITerm, TimedDTerm;

	// Fixed loop integration time in milliseconds
	float DeltaTime;

	bool FirstLoop;
	float LastInput; // Tracking for use with D term
};