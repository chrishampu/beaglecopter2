#include "PacketJSONPayload.h"
#include "NetPlatformLinux.h"
#include "NetSession.h"

PacketJSONPayload::PacketJSONPayload()
{
	packetID = pktJSONPayload;
	packetSize = 3;
}

PacketJSONPayload::PacketJSONPayload(std::string JSON)
{
	packetID = pktJSONPayload;

	this->JSON = JSON;

	PreparePacket();

	setPacketSize(PacketData.bufSize());
}

void PacketJSONPayload::PreparePacket()
{
	WritePacketHeader(packetID);

	PacketData << (short)JSON.length();
	PacketData.write((unsigned char*)JSON.c_str(), JSON.length());
}

void PacketJSONPayload::Handle(NetStream& ns, NetSession* sess)
{
	Decode(ns);

	// What to do once we have a JSON string? No JSON functionality yet!
}

void PacketJSONPayload::Decode(NetStream& ns)
{
	ns.skip(2); // Skip packet header

	short length;
	ns >> length;

	if (length <= 0)
		return;

	JSON.reserve(length);

	// This allocation is inefficient, but the only way to do it
	// since we can't read data directly into a std string (as far as i know)
	unsigned char *data = new unsigned char[length + 1]; // +1 for null termination

	ns.read(data, length);
	data[length] = '\0';

	JSON.append((char*)data);

	delete data;
}