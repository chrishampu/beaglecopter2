#include "NetPlatformLinux.h"
#include "NetPacketManager.h"
#include "NetSessionManager.h"
#include "NetStream.h"

#include <string>
#include <sstream>
#include <vector>
#include <chrono>

#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/poll.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <netinet/in.h>
#include <errno.h>
#include <net/if_ppp.h>
#include <sys/ioctl.h>   /* ioctl() */
#include <net/ppp_defs.h>

static std::vector<NetSocket*> PolledSockets;

#define INVALID_SOCKET -1

#define MaxPacketDataSize 1024

static int defaultPort = 24000;
static bool Initialized = false;

const char defaultAddress[16] = "127.0.0.1";
//const char *destinationAddress = serverAddress; // Temporary

/** In a 2-way TCP handshake, 3 sockets are required.
 ** The first socket is the serverside listen socket.
 ** Second, the socket created by the client in order to
 ** keep track of the client -> server connection.
 ** Third, the socket created by the server when a connection
 ** is accepted from a socket. On this socket, data is sent
 ** from the server -> client, and received from the client.
 ** These 3 sockets describe a SYN->SYN ACK->SYN connection.
 **/

ClientSocketObject *ClientSocketObject::first = NULL;

// We want the latest version of winsock which is 2.2, so use MAKEWORD(2.2)
bool NetPlatformLinux::Init()
{
	if(Initialized == true)
	{
		//Console::Instance()->printf(Networking, "NetPlatformLinux::Init(): Fatal error: Network already initialized!\r\n");
		return false;
	}

	//NetPacketManager::Instance()->Create();
	//NetSessionManager::Instance()->Create();

	CRYPTO_malloc_init();
	SSL_library_init();

	OpenSSL_add_all_algorithms();
	SSL_load_error_strings();

	return true;
}

void NetPlatformLinux::Shutdown()
{
	if(Initialized == true)
	{
		for (NetSocketObject *obj : ListenSockets)
			delete obj;

		ClientSocketObject::ObjectCleanup();

		//NetPacketManager::Instance()->Destroy();
		//NetSessionManager::Instance()->Destroy();

		Initialized = false;
	}
}

void NetPlatformLinux::Process()
{
	int optval = 0;
	socklen_t optlen = sizeof(int);
	int bytesRead;
	NetPlatformLinux::NetError err;
	bool removeSock;
	NetSocket *currentSock = NULL;
	sockaddr_in ipAddr;
	NetSocket incoming = InvalidSocket;
	char out_h_addr[1024];
	int out_h_length = 0;
	unsigned char data[MaxPacketDataSize];
	NetAddress address;
	std::vector<unsigned char> bytesRecv;

	NetSessionManager::SessionMap sessions = NetSessionManager::Instance()->getAllSessions();

	for (NetSessionManager::SessionMap::iterator it = sessions.begin(); it != sessions.end(); ++it)
	{
		removeSock = false;
		NetSession *session = it->second;

		switch (session->ConnectionState)
		{
		case ConnectionStateType::Closed:
			//Console::Instance()->printf("NetPlatformLinux::Process(): Closing session\n");
			printf("Closing connection\n");
			NetSessionManager::Instance()->RemoveSession(session);
			break;
		case ConnectionStateType::InvalidState:
			break;
		case ConnectionStateType::ConnectionPending:
			errno = 0; // TODO: Figure out whats causing an error before this is reached
			if (getsockopt(session->getSocket(), SOL_SOCKET, SO_ERROR, 
				&optval, &optlen) == -1)
			{
				if(session->ShouldReconnect == true)
				{
					session->Reconnect();
					printf("Connection error while getting socket options. Retrying..\n");
				}
				else
				{
					removeSock = true;
					printf("Connection error while getting socket options\n");
				}
			}
			else
			{
				if (optval == EINPROGRESS)
				{
					// still connecting
					break;
				}

				if (optval == 0)
				{
					session->ConnectionState = ConnectionStateType::Connected;
					session->ConnectionAttempts = 0;
					session->LastConnectionAttempt = std::chrono::steady_clock::now();

					printf("Connected successfully %d\n", session->Socket);
				}
				else
				{
					if(session->ShouldReconnect == true)
					{
						session->Reconnect();
						printf("Failed to connect to server. Retrying..\n");
					}
					else
					{
						removeSock = true;
						printf("Failed to connect to server\n");
					}
					
				}
			}
			break;
		case ConnectionStateType::Reconnecting:
			{
				// Put our socket into an invalid state
				if(session->ConnectionAttempts > MaxReconnectionAttempts) 
				{
					session->ConnectionState = ConnectionStateType::Closed;
					printf("Max reconnection attempts reached. Abandoning connection\n");
					break;
				}

				auto Ticks = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::duration<double, std::milli>(ReconnectionTime)).count();

				auto Now = std::chrono::steady_clock::now();
				auto CurrentCount = std::chrono::duration_cast<std::chrono::microseconds>(Now - session->LastConnectionAttempt);

				if(CurrentCount.count() < Ticks)
					break;

				if(session->getSocket() != InvalidSocket)
				{
					printf("Reconnect: Closed socket %d\n", session->Socket);
					closeSocket(session->getSocket());
					session->InvalidateSocket();
				}

				sleep(5);

				session->Socket = openSocket();

				printf("Reconnect: Opened socket %d\n", session->Socket);

				setBlocking(session->getSocket(), false);

				sockaddr_in ipAddr;

				ipAddr.sin_addr.s_addr = inet_addr(session->getAddressString().c_str());
				ipAddr.sin_port = session->getAddress().port;
				ipAddr.sin_family = AF_INET;

				session->ConnectionAttempts++;
				session->LastConnectionAttempt = Now;
				printf("Attempting to reconnect to %s:%d.. %d\n", session->getAddressString().c_str(), session->getAddress().port, session->ConnectionAttempts);

				int error = NetPlatformLinux::Success;
				if(::connect(session->getSocket(), (struct sockaddr *) &ipAddr, sizeof(ipAddr)) < 0)
				{
					error = NetPlatformLinux::getLastError();
					printf("Reconnection error %d %d\n", errno, error);
					if(error != NetPlatformLinux::WouldBlock && error != NetPlatformLinux::ConnectionInProgress) // WouldBlock errors are perfectly normal... I think
					{
						//Console::Instance()->printf(Networking, "NetPlatformLinux::openConnectTo(): Connection error: %d\r\n", error);
						closeConnectTo(session->getSocket());
						printf("Failed to reconnect. Retrying.. %d\n", session->ConnectionAttempts);
						break;
					}
				}

				if(error == ConnectionInProgress)
				{
					session->ConnectionState = ConnectionStateType::ConnectionPending;
				}
				else
				{
					printf("Connected successfully\n");
					session->ConnectionState = ConnectionStateType::Connected;
					session->ConnectionAttempts = 0;
				}
			}
			break;
		case ConnectionStateType::Connected:
			bytesRead = 0;

			if (session->HasSSL == true && session->SSLHandshakeStatus != Completed)
			{

				int res = SSL_accept(session->SSLSocket);
				int ssl_error = SSL_get_error(session->SSLSocket, res);

				if (res == 1)
				{
					session->SSLHandshakeStatus = Completed;
					break;
				}

				if (ssl_error == SSL_ERROR_NONE)
					session->SSLHandshakeStatus = Completed;
				else if (ssl_error == SSL_ERROR_WANT_READ)
					session->SSLHandshakeStatus = RequiresRead;
				else if (ssl_error == SSL_ERROR_WANT_WRITE)
					session->SSLHandshakeStatus = RequiresWrite;
			}
			else
			{
				// SpeciaL case for websockets waiting to handshake
				if(session->PortType == ConnectToPort && session->getSocketType() == WebSocket
					&& session->WebSocketHandshakeStatus == HandshakeRequestSent)
				{
					auto Ticks = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::duration<double, std::milli>(ReconnectionTime)).count();

					auto Now = std::chrono::steady_clock::now();
					auto CurrentCount = std::chrono::duration_cast<std::chrono::microseconds>(Now - session->HandshakeSentTime);

					if(CurrentCount.count() > Ticks)
					{
						printf("Received no websocket response in last %d seconds, forcing reconnect\n", ReconnectionTime/1000);
						session->Reconnect();
						break;
					}
				}

				do
				{
					err = NetPlatformLinux::recv(session, (char*)data, MaxPacketDataSize, &bytesRead);

					if (err == NetPlatformLinux::Success && bytesRead > 0)
					{
						
						//Console::Instance()->printf("NetPlatformLinux::Process(): Read %d bytes\n", bytesRead);
						unsigned char *real = reinterpret_cast<unsigned char*>(data);
						std::vector<unsigned char> temp(real, real + bytesRead);
						bytesRecv.insert(bytesRecv.end(), temp.begin(), temp.end());
					}
					else
						break;
				} while (err == NetPlatformLinux::Success && bytesRead == MaxPacketDataSize);
			}
			
			if(bytesRecv.size() > 0)
			{
				getLastError();
				//Console::Instance()->printf("NetPlatformLinux::Process(): Creating packet\n");
				NetPacket *packetEvent = new NetPacket();

				packetEvent->PacketData.write(bytesRecv.data(), bytesRecv.size());
				packetEvent->setPacketSize(bytesRecv.size());

				session->AddPacket(packetEvent);
			}
			else if (bytesRead == 0)
			{
				printf("NetPlatformLinux::Process(): Connection from %s reset by peer\n", session->getAddressString().c_str());
				
				if(session->ShouldReconnect == true)
				{
					printf("Reconnecting in %d seconds\n", ReconnectionTime/1000);
					session->Reconnect();
				}
				else
					removeSock = true;
			}

			//if (err == NetPlatformLinux::ConnectionReset)
			//{
			//	printf("Closing session\n");
			//	NetSessionManager::Instance()->RemoveSession(session);
			//}
			break;
		case ConnectionStateType::NameLookupRequired:
			//Console::Instance()->printf("NetPlatformLinux::Process(): Name lookup required\n");
			if (out_h_length == -1)
			{
				//Console::Instance()->printf("NetPlatformLinux::Process(): DNS lookup failed: %s\n", session->getAddressString().c_str());
				removeSock = true;
			}
			else
			{
				memcpy(&(ipAddr.sin_addr.s_addr), out_h_addr, out_h_length);
				ipAddr.sin_port = session->getAddress().port;
				ipAddr.sin_family = AF_INET;
				if (::connect(session->getSocket(), (struct sockaddr *)&ipAddr,
					sizeof(ipAddr)) == -1)
				{
					if (errno == EINPROGRESS)
					{
						session->ConnectionState = ConnectionStateType::ConnectionPending;
					}
					else
					{
						printf("NetPlatformLinux::Process(): Error connecting to %s: %s\n",
								session->getAddressString().c_str(), strerror(errno));

						removeSock = true;
					}
				}
				else
				{
					session->ConnectionState = ConnectionStateType::Connected;
				}
			}
			break;
		case ConnectionStateType::Listening:
			{
				NetAddress address;
				incoming = NetPlatformLinux::accept(session->getSocket(), &address);
				if(incoming != InvalidSocket)
				{
					//Console::Instance()->printf(Debug, "NetPlatformLinux::Process(): Incoming connection from %d\n", incoming);
					setBlocking(incoming, false);

					NetSession *newsession = new NetSession(incoming, address);

					NetSocketObject *server = getObjectBySocket(session->getSocket());

					if (server == nullptr)
					{
						//Console::Instance()->printf(Networking, "NetPlatformLinux::Process(): Error initializing new connection from %d\r\n", incoming);
						delete newsession;
					}

					// Initiate SSL handshake if we require it
					if (server->useSSL == true)
					{
						newsession->HasSSL = true;

						newsession->SSLSocket = SSL_new(server->SSLContext);

						if (newsession->SSLSocket == nullptr)
						{
							//Console::Instance()->printf(Networking, "NetPlatformLinux::Process(): Error initializing SSL from connection %d\r\n", incoming);
							delete newsession;
						}

						if (SSL_set_fd(newsession->SSLSocket, incoming) != 1)
						{
							//Console::Instance()->printf(Networking, "NetPlatformLinux::Process(): Error initializing SSL from connection %d\r\n", incoming);
							delete newsession;
						}

						int res = SSL_accept(newsession->SSLSocket);
						int ssl_error = SSL_get_error(newsession->SSLSocket, res);

						if (ssl_error == SSL_ERROR_WANT_READ)
							newsession->SSLHandshakeStatus = RequiresRead;
						else if (ssl_error == SSL_ERROR_WANT_WRITE)
							newsession->SSLHandshakeStatus = RequiresWrite;
					}

					session->ConnectionState = ConnectionStateType::ConnectionPending;
					NetSessionManager::Instance()->AddSession(newsession);

					//Console::Instance()->printf("NetPlatformLinux::Process(): Accepting connection\n");
				}
			}
			break;
		}

		if(removeSock == true)
		{	
			printf("NetPlatformLinux::Process(): Removing session\n");
			NetSessionManager::Instance()->RemoveSession(session);
		}
	}

	NetSessionManager::Instance()->UpdateSessions();
}

NetSocket NetPlatformLinux::openListenPort(const char *address, unsigned short port, bool useSSL)
{
	if (port == 0)
		port = defaultPort;

	for (NetSocketObject *obj : ListenSockets)
	{
		if (obj->port == port)
		{
			//Console::Instance()->printf(ColorCode::Error, "NetPlatformLinux::openListenPort(): We're already listening on port %d!\r\n", port);
			return InvalidSocket;
		}
	}

	NetSocketObject *ServerSocket = new NetSocketObject();

	NetPlatformLinux::NetError err;

	ServerSocket->socket = openSocket();
	ServerSocket->type = Server;
	ServerSocket->useSSL = useSSL;

	//Console::Instance()->printf(Networking, "NetPlatformLinux::openListenPort(): Listen socket: %d\r\n", ServerSocket->socket);

	if(strcmp(address,"") == 0)
		err = bind(ServerSocket->socket, port);
	else
		err = bind(ServerSocket->socket, port, address);

	if(err != NetPlatformLinux::Success)
	{
		delete ServerSocket;
		return InvalidSocket;
	}
	
	if(listen(ServerSocket->socket, 1) != NetPlatformLinux::Success)
	{
		delete ServerSocket;
		return InvalidSocket;
	}

	setBlocking(ServerSocket->socket, false);

	if (ServerSocket->useSSL == true)
	{
		ServerSocket->SSLContext = SSL_CTX_new(SSLv23_server_method());

		if (ServerSocket->SSLContext == nullptr)
		{
			//Console::Instance()->printf(Networking, "NetPlatformLinux::openListenPort(): Unable to secure listen port\n");
			delete ServerSocket;
			return InvalidSocket;
		}

		SSL_CTX_use_certificate_file(ServerSocket->SSLContext, "keys/dummy.crt", SSL_FILETYPE_PEM);
		SSL_CTX_use_PrivateKey_file(ServerSocket->SSLContext, "keys/dummy.key", SSL_FILETYPE_PEM);

		if (!SSL_CTX_check_private_key(ServerSocket->SSLContext))
		{
			//Console::Instance()->printf(Networking, "NetPlatformLinux::openListenPort(): Unable to secure listen port\n");
			delete ServerSocket;
			return InvalidSocket;
		}
	}

	{
		ServerSocket->port = port;
	
		// In Linux, the socket has to be polled, so we add it as a session
		NetAddress naddress;
		NetSession *session = new NetSession(ServerSocket->getSocket(), naddress);
		session->ConnectionState = ConnectionStateType::Listening;
		session->PortType = NetSessionPortType::ListenPort;
		NetSessionManager::Instance()->AddSession(session);

		// and give it a special state.
		if(address == "")
		{
			sockaddr_in addr;
			unsigned int len = sizeof(addr);
			getsockname(ServerSocket->socket, (sockaddr*)&addr, &len);
			//Console::Instance()->printf(Networking, "NetPlatformLinux::openListenPort(): Server is listening on %s:%d%s\r\n", inet_ntoa(addr.sin_addr), port, ServerSocket->useSSL == true ? " with ssl enabled" : "");
		}
		//else
			//Console::Instance()->printf(Networking, "NetPlatformLinux::openListenPort(): Server is listening on %s:%d%s\r\n", address, port, ServerSocket->useSSL == true ? " with ssl enabled":"");
	}

	ListenSockets.push_back(ServerSocket);

	return ServerSocket->socket;
}

NetSocket NetPlatformLinux::openConnectTo(char *stringAddress, unsigned short port, NetSessionType RequestedSessionType, bool Reconnect)
{
	// Create our socket object.
	// Our objects constructor will add it to a linked list so it won't go out of scope.
	ClientSocketObject *ClientSocket = new ClientSocketObject();

	ClientSocket->socket = openSocket();
	ClientSocket->port = port;

	if(ClientSocket->socket == InvalidSocket)
	{
		return InvalidSocket;
	}

	setBlocking(ClientSocket->socket, false);

	sockaddr_in ipAddr;

	char listenAddress[16];

	// If address is specified as auto, then try to auto detect the local adapter
	if(strncmp(stringAddress, "auto", 4) == 0)
	{
		// Resolve the IP we'll connect to
		if(detectLocalAddress(listenAddress) == NetPlatformLinux::Success)
			ipAddr.sin_addr.s_addr = inet_addr(listenAddress);
		else
			ipAddr.sin_addr.s_addr = INADDR_NONE;
	}
	else
		ipAddr.sin_addr.s_addr = inet_addr(stringAddress);

	if(ipAddr.sin_addr.s_addr != INADDR_NONE)
	{
		ipAddr.sin_port = port == 0 ? htons(defaultPort) : htons(port);
		ipAddr.sin_family = AF_INET;

		printf("NetPlatformLinux::openConnectTo(): Connecting to %s:%d\r\n", strcmp(stringAddress, "auto")==0 ? listenAddress : stringAddress, port == 0 ? defaultPort : port);
		int error = NetPlatformLinux::Success;
		if(::connect(ClientSocket->socket, (struct sockaddr *) &ipAddr, sizeof(ipAddr)) < 0)
		{
			error = NetPlatformLinux::getLastError();
			if(error != NetPlatformLinux::WouldBlock && error != NetPlatformLinux::ConnectionInProgress) // WouldBlock errors are perfectly normal... I think
			{
				//Console::Instance()->printf(Networking, "NetPlatformLinux::openConnectTo(): Connection error: %d\r\n", error);
				closeConnectTo(ClientSocket->socket);
				delete ClientSocket;
				return InvalidSocket;
			}
		}

		NetAddress naddress;

		naddress.type = NetAddress::IPAddress;
		naddress.port = port == 0 ? defaultPort : port;

		char *tAddr = inet_ntoa(ipAddr.sin_addr);
		unsigned char nets[4];
		nets[0] = atoi(strtok(tAddr, "."));
		nets[1] = atoi(strtok(NULL, "."));
		nets[2] = atoi(strtok(NULL, "."));
		nets[3] = atoi(strtok(NULL, "."));

		naddress.netNum[0] = nets[0];
		naddress.netNum[1] = nets[1];
		naddress.netNum[2] = nets[2];
		naddress.netNum[3] = nets[3];

		NetSession *session = new NetSession(ClientSocket->socket, naddress);
		session->ConnectionState = ConnectionStateType::Listening;
		session->PortType = NetSessionPortType::ConnectToPort;
		session->setSocketType(RequestedSessionType);
		session->ShouldReconnect = Reconnect;
		NetSessionManager::Instance()->AddSession(session);

		if(error == ConnectionInProgress)
		{
			printf("Connection in progress\n");
			session->ConnectionState = ConnectionStateType::ConnectionPending;
		}
		else
		{
			printf("Connected successfully\n");
			session->ConnectionState = ConnectionStateType::Connected;
			session->ConnectionAttempts = 0;
			session->LastConnectionAttempt = std::chrono::steady_clock::now();
		}

		return ClientSocket->socket;
	}
	else 
	{
		//Console::Instance()->printf(Networking, "NetPlatformLinux::openConnectTo(): Invalid address\r\n");
		delete ClientSocket;
	}

	return InvalidSocket;
}

void NetPlatformLinux::closeConnectTo(NetSocket socket)
{
	closeSocket(socket);
	socket = InvalidSocket;
}

// One feature I can add later is turning bufferSize into a reference pointer and modifying
// it to the amount of data sent during the ::send process. That way, the caller method
// can get the exact number of bytes sent relative to how much data was sent. Not sure if 
// this'll be worth the effort though, albeit a minimal amount of effort.
NetPlatformLinux::NetError NetPlatformLinux::sendtoSocket(NetSession *session, const unsigned char *buffer, int bufferSize)
{
	if (session->getSocket() == InvalidSocket)
	{
		return NetPlatformLinux::NotASocket;
	}
	else if (session->ConnectionState != ConnectionStateType::Connected)
	{
		return NetPlatformLinux::ConnectionInProgress;
	}

	// When data is sent, sends return value is equal to the amount of bytes sent.
	// When an error occurs, the return value is instead SOCKET_ERROR, and the
	// exact error code can be obtained through getLastError()
	// My initial assumption was that the return value is directly an error number
	// which explains the phenomenom I was experiencing with return values.

	if(send(session, (const char*)buffer, bufferSize) == -1)
	{
		printf("sendToSocket: Error sending data\n");
		return getLastError();
	}

	return NetPlatformLinux::Success;
}

NetSocket NetPlatformLinux::openSocket()
{
   int retSocket;
   retSocket = socket(AF_INET, SOCK_STREAM, 0);

   if(retSocket == INVALID_SOCKET)
	  return InvalidSocket;
   else
	  return retSocket;
}

NetPlatformLinux::NetError NetPlatformLinux::closeSocket(NetSocket socket)
{
	if(socket != InvalidSocket)
	{
		if(close(socket) == 0)
		{
			printf("Closed connecion\n");
			socket = InvalidSocket;
			return Success;
		}
		else
		{
			socket = InvalidSocket;
			return getLastError();
		}
	}

	return NotASocket;
}

NetPlatformLinux::NetError NetPlatformLinux::getLastError()
{
	switch(errno)
	{
		case 0: // If we're error checking but theres no actual error, I guess we can just assume success
			return NetPlatformLinux::Success;
		case EAGAIN:
			return NetPlatformLinux::WouldBlock;
		case EINPROGRESS:
			return NetPlatformLinux::ConnectionInProgress;
		default:
			printf("NetPlatformLinux::getLastError(): Unhandled socket error %d\n", errno);
			return NetPlatformLinux::UnknownError;
	}

	return NetPlatformLinux::UnknownError;
}

std::string NetPlatformLinux::NetErrorToString(const NetError error)
{
	return "";
}

std::string NetPlatformLinux::NetAddressToString(const NetAddress *address)
{
	std::string out;

	if(address->type == NetAddress::IPAddress)
	{
		std::ostringstream stream;

		stream << (int)address->netNum[0] << '.' << (int)address->netNum[1] << '.' << (int)address->netNum[2] << '.' << (int)address->netNum[3];

		out = stream.str();
	}

	return out;
}

NetPlatformLinux::NetError NetPlatformLinux::send(NetSession *session, const char *buffer, int bufferSize)
{
	int error;

	if (session->HasSSL == true)
		error = SSL_write(session->SSLSocket, (const char*)buffer, bufferSize);
	else
		error = ::send(session->getSocket(), (const char*)buffer, bufferSize, 0);

   if(!error)
	  return Success;
   return getLastError();
}

NetPlatformLinux::NetError NetPlatformLinux::recv(NetSession *session, char *buffer, int bufferSize, int *bytesRead)
{
	if (session->HasSSL == true)
		*bytesRead = SSL_read(session->SSLSocket, (char*)buffer, bufferSize);
	else
		*bytesRead = ::recv(session->getSocket(), (char*)buffer, bufferSize, 0);

   if(*bytesRead == 0) // Return value of 0 means connection was shutdown
	  return NetPlatformLinux::ConnectionReset;
   else if(*bytesRead == -1)
	return NetPlatformLinux::WouldBlock;
 
   return Success;
}

NetPlatformLinux::NetError NetPlatformLinux::connect(NetSocket socket, const NetAddress *address)
{
	return Success;
}

NetPlatformLinux::NetError NetPlatformLinux::listen(NetSocket socket, int maxConcurrentListens)
{
   if(!::listen(socket, maxConcurrentListens))
	  return Success;
   return getLastError();
}

NetSocket NetPlatformLinux::accept(NetSocket acceptSocket, NetAddress *remoteAddress)
{
   sockaddr_in socketAddress;

   unsigned int addrLen = sizeof(socketAddress);

   int retVal = ::accept(acceptSocket, (struct sockaddr *) &socketAddress, &addrLen);
   if(retVal != INVALID_SOCKET)
   {
	   remoteAddress->type = NetAddress::IPAddress;
	   remoteAddress->port = htons(socketAddress.sin_port);

	   char *tAddr;
	   tAddr = inet_ntoa(socketAddress.sin_addr);
	   unsigned char nets[4];
	   nets[0] = atoi(strtok(tAddr, "."));
	   nets[1] = atoi(strtok(NULL, "."));
	   nets[2] = atoi(strtok(NULL, "."));
	   nets[3] = atoi(strtok(NULL, "."));

	   remoteAddress->netNum[0] = nets[0];
	   remoteAddress->netNum[1] = nets[1];
	   remoteAddress->netNum[2] = nets[2];
	   remoteAddress->netNum[3] = nets[3];

	  return retVal;
   }
   return InvalidSocket;
}

NetPlatformLinux::NetError NetPlatformLinux::detectLocalAddress(char *address)
{
	strncpy(address, defaultAddress, 16);
	return NetPlatformLinux::Success;
}

NetPlatformLinux::NetError NetPlatformLinux::bind(NetSocket socket, unsigned short port, const char *address)
{
	int error = NetPlatformLinux::BadAddress;

	sockaddr_in socketAddress;

	memset((char *)&socketAddress, 0, sizeof(socketAddress));
	socketAddress.sin_family = AF_INET;
	socketAddress.sin_port = htons(port);
	char serverAddress[16];
	
	if(address == "" || strcmp(address, "auto") == 0)
	{
		if(detectLocalAddress(serverAddress) == NetPlatformLinux::Success)
		{
			socketAddress.sin_addr.s_addr = inet_addr( serverAddress );
		}
		else
		{
			socketAddress.sin_addr.s_addr = INADDR_ANY;
		}
	}
	else
	{
		socketAddress.sin_addr.s_addr = inet_addr( address );
	}

   error = ::bind(socket, (struct sockaddr *) &socketAddress, sizeof(socketAddress));

   if(error != -1)
		return Success;
   return getLastError();
}

NetPlatformLinux::NetError NetPlatformLinux::setBufferSize(NetSocket socket, int bufferSize)
{
	return Success;
}

NetPlatformLinux::NetError NetPlatformLinux::setBroadcast(NetSocket socket, bool broadcastEnable)
{
	return Success;
}

NetPlatformLinux::NetError NetPlatformLinux::setBlocking(NetSocket socket, bool blockingIO)
{
	int notblock = !blockingIO;
	int error = ioctl(socket, FIONBIO, &notblock);
   
   if(!error)
	  return Success;
   return getLastError();
}