#pragma once

#include "PID.h"
#include <string>
#include <atomic>

// Forward declare the FlightController so it can access private methods
template<typename FlightPolicy>
class FlightController;

// Base class for flight policies that defines the interface
class Flight
{
	// And friend class the FlightController
	template<typename FlightPolicy> friend class FlightController;

public: 
	Flight();
	Flight(const Flight& Other);
	
	~Flight();

	const PID& GetYawPID()   const { return YawPID; }
	const PID& GetPitchPID() const { return PitchPID; }
	const PID& GetRollPID()  const { return RollPID; }

	virtual std::string ToString() const = 0;

protected:
	PID YawPID;
	PID PitchPID;
	PID RollPID;

	std::atomic<bool> ContinueFlightMode;
	std::atomic<bool> TuningParametersChanged;
};

// Describes a flight policy where motors are off and plane does nothing
class GroundedFlight : public Flight
{
public:
	GroundedFlight();
	~GroundedFlight();

	void FlightLoop();

	std::string ToString() const { return "Grounded Flight"; }

private:
};

// Describes a flight where the UAV is in the air and must run a PID loop and update motors
class StabilizedFlight : public Flight
{
public:
	StabilizedFlight();
	~StabilizedFlight();

	void FlightLoop();

	std::string ToString() const { return "Stabilized Flight"; }

private:
};

// Describes a flight where the UAV is attempting to decreaase throttle and land smoothly
class LandingFlight : public Flight
{
public:
	LandingFlight();
	~LandingFlight();

	void FlightLoop();

	std::string ToString() const { return "Landing Flight"; }

private:
};