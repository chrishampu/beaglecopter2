#pragma once

#include "NetSocket.h"
#include "NetPacketList.h"

#include <queue>
#include <bitset>
#include <chrono>

static int MaxReconnectionAttempts = 10; // Number of tries before destroying session
static int ReconnectionTime = 4000;  // Time in ms between connection attempts

enum NetSessionPortType {
	ListenPort,
	ConnectToPort
};

enum NetSessionType {
	Undetermined = 0,
	WebSocket,
	TCPSocket,
	HTTPSocket
};

enum ConnectionStateType {
	InvalidState = -1,
	ConnectionPending,
	Connected,
	NameLookupRequired,
	Listening,
	Closed,
	Reconnecting
};

enum SessionFlagType {
	RequiresGPSInfo = 0,
	RequiresCameraImages,
	RequiresPositionInfo,
	HasMissionPlanner,
	SessionFlagLast
};

enum SSLHandshakeState {
	Incomplete = -1,
	Completed,
	RequiresRead,
	RequiresWrite
};

enum WebSocketHandshakeState {
	HandshakeIncomplete = -1,
	HandshakeRequestSent,
	HandshakeRequestReceived,
	HandshakeComplete
};

class NetSession
{
public:
	NetSession(NetSocket sock, NetAddress addr);
	~NetSession();

	NetSession(const NetSession&) = delete;
	NetSession(NetSession&&) = delete;
	NetSession& operator=(NetSession) = delete;
	NetSession& operator=(NetSession&&) = delete;

	bool Update();
	void Reconnect();
	void InvalidateSocket();

	void AddPacket(NetPacket *packet);

	ConnectionStateType ConnectionState;
	NetSessionPortType PortType;
	short Port;

	bool ShouldReconnect;
	int ConnectionAttempts;
	std::chrono::time_point<std::chrono::steady_clock> LastConnectionAttempt;

	NetSocket Socket;

private:
	struct SessionStateT
	{
		friend class NetSession;
	protected:
		std::bitset<SessionFlagLast> SessionFlags;

		NetSession *parent;
	} SessionState;

	NetSessionType SocketType;
	NetAddress Address;

	typedef std::queue<NetPacket*> PacketQueueT;
	PacketQueueT PacketQueue;

public:
	// For discerning SSL sessions
	bool HasSSL;
	SSLHandshakeState SSLHandshakeStatus;
	SSL *SSLSocket;

	// WebSocket-related variables
	WebSocketHandshakeState WebSocketHandshakeStatus;
	std::chrono::time_point<std::chrono::steady_clock> HandshakeSentTime;

	// Misc set/get functions
	NetSocket getSocket() { return Socket; }
	NetAddress getAddress() { return Address; }
	NetSessionType getSocketType() { return SocketType; }
	void setSocketType(NetSessionType type) { SocketType = type; }

	SessionStateT &getSessionState() { return SessionState; };
	bool getSessionFlag(SessionFlagType flag) { return SessionState.SessionFlags[flag]; }

	std::string getAddressString();
};
