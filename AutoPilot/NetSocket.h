#pragma once

#include <openssl/ssl.h>
#include <vector>
#include <sstream>
#include <unistd.h>

// TODO: Refactor the socket classes into just a single class, and optionally implement a manager class

typedef int NetSocket;
const NetSocket InvalidSocket = -1;

enum SockType
{
	BadSocket = -1,
	Server,
	Client
};

struct NetAddress 
{
	int type;        ///< Type of address (IPAddress currently)

	/// Acceptable NetAddress types.
	enum 
	{
		IPAddress
	};

	unsigned char netNum[4];    ///< For IP:  sin_addr<br>
	char nodeNum[6];   ///< For IP:  Not used.<br>
	short  port;       ///< For IP:  sin_port<br>

	inline std::string getIPString()
	{
		std::ostringstream stream;

		stream << (int)netNum[0] << '.' << (int)netNum[1] << '.' << (int)netNum[2] << '.' << (int)netNum[3];

		return stream.str();
	}

	inline bool operator==(NetAddress other)
	{
		return netNum[0] == other.netNum[0] && netNum[1] == other.netNum[1] && netNum[2] == other.netNum[2] && netNum[3] == other.netNum[3];
	}
};

/**
	This is the base class for a SocketObject.
	It's got a basic constructor/destructor, and holds information
	for the socket, its type, and the associated origin address
*/
class NetSocketObject
{
public:
	NetSocketObject()
		: address(NetAddress())
	{
		type = BadSocket;
		socket = InvalidSocket;
		useSSL = false;
		SSLContext = nullptr;
		port = 0;
	}

	~NetSocketObject()
	{
		if(socket != BadSocket)
#ifdef PLATFORM_WIN
			closesocket(socket);
#else
			close(socket);
#endif

		if (useSSL == true && SSLContext != nullptr)
			SSL_CTX_free(SSLContext);
	}

	NetSocket getSocket()
	{
		return socket;
	}

	bool useSSL;
	SSL_CTX *SSLContext;

	unsigned short port;

	NetSocket socket;
	SockType type;

	NetAddress address;
};

/** 
	Here we have a Client Socket Object that implements our base class.
	This class is used to create a fake client model. It uses a linked list
	to help us keep track of all our client objects. It also implements
	a second NetSocket, which is used by the server for identification.
	The socket in the base class is the real clientside socket.

	This is only temporary in order to emulate a client/server model
	in the same program for simplicities sake.
*/
class ClientSocketObject : public NetSocketObject
{
public:
	ClientSocketObject()
	{
		socket = BadSocket;
		serverSocket = BadSocket;
		type = Client;
		next = first;
		first = this;
	}

	~ClientSocketObject()
	{
		if(socket != BadSocket)
#ifdef PLATFORM_WIN
			closesocket(socket);
#else
			close(socket);
#endif
	}

	// Return an object given a specific socket. Useful during
	// read events when we need to figure out where a particular
	// event came from.
	static ClientSocketObject* getClientObject(NetSocket sock)
	{
		for(ClientSocketObject *needle = first; needle; needle = needle->next)
			if(needle->socket == sock || needle->serverSocket == sock)
				return needle;

		return NULL;
	}

	static ClientSocketObject* getFirstClient()
	{
		return first == NULL ? NULL:first;
	}

	static void ObjectCleanup()
	{
		if(first == NULL)
			return;

		ClientSocketObject *needle = first->next;
		ClientSocketObject *temp = NULL;

		while(needle != NULL)
		{
			temp = needle->next;
			delete needle;
			needle = temp;
		}

		delete first;
	}

	NetSocket serverSocket; // Server side socket

private:
	ClientSocketObject *next;
	static ClientSocketObject *first;
};
