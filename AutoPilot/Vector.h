#pragma once

class Vector
{
public:
	Vector() : x(0), y(0), z(0) {}
	Vector(double unit) : x(unit), y(unit), z(unit) {}
	Vector(double X, double Y, double Z) : x(X), y(Y), z(Z) {}

	Vector(const Vector& other)
	{
		x = other.x;
		y = other.y;
		z = other.z;
	}

	void operator=(const Vector& other)
	{
		x = other.x;
		y = other.y;
		z = other.z;		
	}

	// To help with remembering what the x,y,z corresponds to
	float roll() { return x; }
	float pitch() { return y; }
	float yaw() { return z; }

	double x, y, z;
};