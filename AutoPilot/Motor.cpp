#include "Motor.h"
#include <iostream>
#include <stdexcept>
#include <exception>
#include <string>

Motor::Motor(std::string File, MotorDirection Heading)
	: Duty(0)
	, Running(false)
	, Armed(false)
{
	Direction = Heading;

	try
	{
		DutyFile.open(std::string(File).append("/duty"), std::ios::in|std::ios::out);
		RunFile.open(std::string(File).append("/run"), std::ios::in|std::ios::out);

		if(!DutyFile.is_open() || !RunFile.is_open())
			throw std::runtime_error("Failed to open files of motor " + File);

		// Set up defaults
		DutyFile << 0 << std::endl;
		RunFile << 0 << std::endl;
	}
	catch (std::exception& e)
	{
		std::terminate();
	}
}

Motor::~Motor()
{
	if(Running == true)
		Stop();

	RunFile.close();
	DutyFile.close();
}

// Throttle range is 0 ~ 1000 which can be represented as 0.0% ~ 100.0%
void Motor::SetThrottle(unsigned int NewThrottle)
{
	//std::lock_guard<std::mutex> lock(MotorMutex);

	if(NewThrottle > 1000)
		throw std::out_of_range("Throttle on motor is out of range");

	// New actual duty range is 0~50 so adjust accordingly
	Duty = 0 + (float)NewThrottle / 20.0f;

	try
	{
		DutyFile << Duty << std::endl;
	}
	catch(std::exception& e)
	{
		DutyFile << 0 << std::endl;
		std::terminate();
		// TODO: Emergency exception handling
	}
}

float Motor::GetThrottle() 
{ 
	//std::lock_guard<std::mutex> lock(MotorMutex);
	
	return Duty * 20.0f;
}

void Motor::Arm()
{
	SetThrottle(0);

	Run();
}

void Motor::Run()
{
	Running = true;
	RunFile << Running << std::endl;
}

void Motor::Stop()
{
	Running = false;
	RunFile << Running << std::endl;
}