#pragma once

#include "NetPacket.h"
#include "NetPacketList.h"
#include "NetSession.h"
#include "Instanced.h"
#include <map>

typedef void (*PacketHandle)(NetStream& b, NetSession* sess);

class NetPacketManager : public Instanced<NetPacketManager>
{
public:
    NetPacketManager();
    ~NetPacketManager();

	NetPacketManager(const NetPacketManager&) = delete;
	NetPacketManager(NetPacketManager&&) = delete;
	NetPacketManager& operator=(NetPacketManager) = delete;
	NetPacketManager& operator=(NetPacketManager&&) = delete;

    NetPacketManager(unsigned short id, PacketHandle handle);

    void processPacketReceivedEvent(NetPacket *packet, NetSession *session);

	void sendPacketSignal(NetPacket &packet);

    void Register(unsigned short id, PacketHandle handle);

    void Create();
    void Destroy();
};