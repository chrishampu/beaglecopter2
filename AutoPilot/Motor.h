#pragma once

#include <string>
#include <fstream>
#include <mutex>

enum MotorDirection
{
	North,
	East,
	South,
	West
};

class Motor
{
	friend class MotorController;
public:
	Motor(std::string File, MotorDirection Heading);
	~Motor();

	void SetThrottle(unsigned int NewThrottle);

	float GetThrottle();

	MotorDirection GetDirection() { return Direction; }

protected:
	void Arm();

	void Run();
	void Stop();

private:
	MotorDirection Direction;

	std::fstream DutyFile;
	std::fstream RunFile;

	float Duty;
	bool Running;

	bool Armed;

	std::mutex MotorMutex;
};