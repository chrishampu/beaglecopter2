#pragma once

#include "NetStream.h"
#include <stdio.h>

class NetSession;

// All packets have to be distinguished somehow
enum PacketID
{
	pktJSONPayload = 0x100
};

/**
	NetPacket is going to be how data is transferred between the server and the client.
	Each packet can be sent either way, or only one way. The freedom lies in this base class
	which declares each of its methods as virtual. Armed with a dynamic buffer that holds numerous
	data types, a new class can implement this base class and override any functions it needs
	for itself. Then, the new packet class can declare its own data types it may need to fill
	its buffer, and be sent off. The packet ID can distinguish sent and recieved data. When a
	packet is recieved, that recieved buffer can be decoded by that same packet class into
	individual data types. That information can then be used by the recieving client/server.
*/
class NetPacket
{
public:
	NetPacket()
	{
		packetID = 0;
		packetSize = 0;
	}

	NetPacket(const NetPacket&) = delete;
	NetPacket(NetPacket&&) = delete;
	NetPacket& operator=(NetPacket) = delete;
	NetPacket& operator=(NetPacket&&) = delete;

	virtual void PreparePacket()
	{
		WritePacketHeader(packetID);
	}

	virtual void WritePacketHeader(unsigned short id)
	{
		PacketData << id;
	}

	virtual short GetPacketHeader()
	{
		return packetID;	
	}

	virtual void Handle(NetStream& ns, NetSession* sess)
	{
		Decode(ns);
	}

	virtual void Decode(NetStream&)
	{

	}

	virtual void setPacketSize(unsigned int size)
	{
		packetSize = size;
	}

	virtual const unsigned char* getPacketBuffer()
	{
		return PacketData.getBuffer();
	}

	virtual unsigned int getPacketSize()
	{
		return packetSize;
	}

	NetStream PacketData;

protected:
	unsigned short packetID;
	unsigned int packetSize;
};