#pragma once

#include <string>

// Base response template
template<int T>
class HTTPResponse
{
public:
	static const int Code;
	static const std::string ResponseString;

	static const std::string CodeToString()
	{
		return std::to_string(Code);
	}
};

// Specializations
template<> const int HTTPResponse<200>::Code = 200;
template<> const std::string HTTPResponse<200>::ResponseString = "200 OK";
using HTTP200 = HTTPResponse < 200 > ;

template<> const int HTTPResponse<404>::Code = 404;
template<> const std::string HTTPResponse<404>::ResponseString = "404 Not Found";
using HTTP404 = HTTPResponse < 404 > ;

template<> const int HTTPResponse<101>::Code = 101;
template<> const std::string HTTPResponse<101>::ResponseString = "101 Switching Protocols";
using HTTP101 = HTTPResponse < 101 > ;