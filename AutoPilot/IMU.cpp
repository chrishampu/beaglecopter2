#include "IMU.h"
#include "NetWebsocketParser.h"
#include <fcntl.h>
#include <termios.h>
#include <iostream> 
#include <exception>
#include <vector>
#include <cstring>
#include <iomanip>
#include <cmath>
#include <unistd.h>
#include <chrono>
#include <thread>
#include <exception>

static int IMUUpdateFrequency = 66; // 66ms = ~15 updates per second

std::mutex OrientationLock;

IMU::IMU()
	: Instanced()
	, Orientation(0)
{
	struct termios options;

	// First open serial connection and verify it
	try
	{
		serialHandle = open("/dev/ttyO1",O_RDWR|O_NOCTTY|O_NONBLOCK);

		if(serialHandle < 0)
			throw std::runtime_error("Failed to connect to IMU");
	}
	catch (std::exception& e)
	{
		std::terminate();
	}

	// Now set baud rate and important terminal flags
	fcntl(serialHandle, F_SETFL, FNDELAY);
	
	tcgetattr(serialHandle, &options); /* Get the current options for the port */
	
	cfsetispeed(&options, B115200); /* Set the baud rates to 115200 */
	cfsetospeed(&options, B115200);
	options.c_cflag |= (CLOCAL | CREAD); /* Enable the receiver and set local mode */
	options.c_cflag &= ~PARENB; /* Mask the character size to 8 bits, no parity */
	options.c_cflag &= ~CSTOPB;
	options.c_cflag &= ~CSIZE;
	options.c_cflag |= CS8; /* Select 8 data bits */
	options.c_cflag &= ~CRTSCTS; /* Disable hardware flow control */
	options.c_lflag &= ~(ICANON | ECHO | ISIG);/* Enable data to be processed as raw input */
	options.c_cc[VMIN] = 0;
	options.c_cc[VTIME] = 0;		
	
	tcsetattr(serialHandle, TCSANOW, &options); /* Set the new options for the port */

	std::cout << "IMU connected" << std::endl;
}

IMU::~IMU()
{
	close(serialHandle);
}

void IMU::Calibrate()
{
	auto Last = std::chrono::steady_clock::now();

	std::cout << "Calibrating IMU" << std::endl;

	try
	{
		// Spin for 1 second while we gather readings
		while(std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::steady_clock::now() - Last).count() 
			< std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::duration<double, std::milli>(1000)).count())
		{
			Poll();
		}
	}
	catch(std::exception& e)
	{
		std::terminate();
	}

	std::cout << "Calibration finished" << std::endl;
}

void IMU::Poll()
{
	char Buffer[1];
	 
	int readSize;
	
	std::string line;
	static std::string last_line;
	
	readSize = read(serialHandle, Buffer, 1);
	
	if( readSize > 0 )
	{
		if(Buffer[0] == '!' && last_line.length() > 1)
		{
			ProcessSensorString(last_line);
			last_line.erase(0, last_line.length());
		}
		else
		{
			line = Buffer[0];
			last_line.append(line);
		}
	}
}

Vector IMU::GetOrientation()
{
	std::lock_guard<std::mutex> lock(OrientationLock); 

	return Orientation;
}

void IMU::SetOrientation(Vector NewOrientation)
{
	std::lock_guard<std::mutex> lock(OrientationLock);

	Orientation = NewOrientation;
}

void IMU::ProcessSensorString(std::string Line)
{
	std::vector<char*> tokens;
	char *pch;

	pch = strtok((char*)Line.c_str(), ",");
	tokens.push_back(pch);

	while(pch != NULL)
	{
		pch = strtok(NULL, ",");
		tokens.push_back(pch);
	}

	Vector Position(atof(tokens[1]), atof(tokens[2]), atof(tokens[3]));

	SetOrientation(Position);

	Temperature = atof(tokens[4]) / 10.0f;
	Altitude = atof(tokens[5]);

	auto Ticks = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::duration<double, std::milli>(IMUUpdateFrequency)).count();

	auto Now = std::chrono::steady_clock::now();
	auto CurrentCount = std::chrono::duration_cast<std::chrono::microseconds>(Now - LastUpdate);

	if(CurrentCount.count() > Ticks)
	{
		NetWebsocketParser::Instance()->SendTelemetry(Position, Temperature, Altitude);

		LastUpdate = Now;

		//std::cout << std::fixed << std::setprecision(2) << "X: " << Position.x << " Y: " << Position.y << " Z: " << Position.z << " T: " << Temperature << " A: " << Altitude << std::endl;
	}

}