#pragma once

#include "NetSession.h"
#include "Instanced.h"
#include <unordered_map>

class NetSessionManager : public Instanced<NetSessionManager>
{
public:
	NetSessionManager();
	~NetSessionManager();

	NetSessionManager(const NetSessionManager&) = delete;
	NetSessionManager(NetSessionManager&&) = delete;
	NetSessionManager& operator=(NetSessionManager) = delete;
	NetSessionManager& operator=(NetSessionManager&&) = delete;

	void ClearSessions();
	void UpdateSessions();

	NetSession* FindSession(NetSocket sock);
	NetSession* FindSession(NetAddress addr);

	void AddSession(NetSession *session);
	void RemoveSession(NetSession *session);

	typedef std::unordered_map<NetSocket, NetSession*> SessionMap;
	SessionMap getAllSessions() { return Sessions; }

private:
	SessionMap Sessions;
};