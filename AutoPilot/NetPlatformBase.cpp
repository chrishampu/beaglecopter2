#include "NetPlatformBase.h"

NetSocketObject *NetPlatformBase::getObjectBySocket(NetSocket socket)
{
	for (NetSocketObject *obj : ListenSockets)
	{
		if (obj->socket == socket)
			return obj;
	}

	return nullptr;
}