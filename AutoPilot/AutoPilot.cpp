#include "AutoPilot.h"
#include "IMU.h"
#include "MotorController.h"
#include "NetWebsocketParser.h"
#include <iostream>

static int AutoPilotUpdateFrequency = 66; // 66ms = ~15 updates per second

std::mutex ThrottleLock;
std::atomic<float> AdjustedYawThrottle;
std::atomic<float> AdjustedPitchThrottle;
std::atomic<float> AdjustedRollThrottle;

// Init default controller
AutoPilot::AutoPilot()
	: Controller(new FlightController<GroundedFlight>())
	, HeadingHold(0.f)
	, Throttle(0.0f)
{
	AdjustedYawThrottle.store(0.f);
	AdjustedPitchThrottle.store(0.f);
	AdjustedRollThrottle.store(0.f);

	TuningPTerm.store(0.5f);
	TuningITerm.store(1.0f);
	TuningDTerm.store(0.5f);
}

AutoPilot::~AutoPilot()
{
	
}

void AutoPilot::Update()
{
	auto Ticks = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::duration<double, std::milli>(AutoPilotUpdateFrequency)).count();

	auto Now = std::chrono::steady_clock::now();
	auto CurrentCount = std::chrono::duration_cast<std::chrono::microseconds>(Now - LastUpdate);

	if(CurrentCount.count() > Ticks)
	{
		NetWebsocketParser::Instance()->SendPID(AdjustedYawThrottle.load(), AdjustedPitchThrottle.load(), AdjustedRollThrottle.load());

		LastUpdate = Now;
	}
}

void AutoPilot::SetThrottle(float NewThrottle)
{
	std::lock_guard<std::mutex> lock(ThrottleLock);

	Throttle = NewThrottle;

	CommitThrottle();
}

void AutoPilot::UpdateThrottleAdjustments(float Yaw, float Pitch, float Roll)
{
	std::lock_guard<std::mutex> lock(ThrottleLock);

	AdjustedYawThrottle.store(Yaw);
	AdjustedPitchThrottle.store(Pitch);
	AdjustedRollThrottle.store(Roll);

	CommitThrottle();
}

void AutoPilot::CommitThrottle()
{
	unsigned int YawThrottle = (unsigned int)AdjustedYawThrottle;
	unsigned int PitchThrottle = (unsigned int)AdjustedPitchThrottle;
	unsigned int RollThrottle = (unsigned int)AdjustedRollThrottle;

	unsigned int AN = PitchThrottle - YawThrottle;
	unsigned int AS = PitchThrottle - YawThrottle;
	unsigned int AE = RollThrottle + YawThrottle;
	unsigned int AW = RollThrottle + YawThrottle;;

	int N = (unsigned int)Throttle + AN;
	int S = (unsigned int)Throttle - AS;
	int E = (unsigned int)Throttle + AE;
	int W = (unsigned int)Throttle - AW;

	//std::cout << "Y P R = (" << YawThrottle << ", " << PitchThrottle << ", " << RollThrottle << ")   "
	//	 << "NESW = " << N << " " << E << " " << S << " " << W << std::endl;

	if(N < 0)
		N = 0;
	if(N > 900)
		N = 900;

	if(S < 0)
		S = 0;
	if(S > 900)
		S = 900;

	if(E < 0)
		E = 0;
	if(E > 900)
		E = 900;

	if(W < 0)
		W = 0;
	if(W > 900)
		W = 900;

	if(Throttle < 100.f)
	{
		if(N < Throttle)
			N = Throttle;
		if(S < Throttle)
			S = Throttle;
		if(E < Throttle)
			E = Throttle;
		if(W < Throttle)
			W = Throttle;
	}

	MotorController::Throttle<North>::Set(N);
	MotorController::Throttle<South>::Set(S);
	MotorController::Throttle<East>::Set(E);
	MotorController::Throttle<West>::Set(W);
}