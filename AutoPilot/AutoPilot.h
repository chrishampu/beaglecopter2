#pragma once

#include "Instanced.h"
#include "FlightController.h"
#include <exception>
#include <memory>
#include <mutex>
#include <atomic>
#include <chrono>

// Overseer of flight mode transitions
class AutoPilot : public Instanced<AutoPilot>
{
public:
	AutoPilot();
	~AutoPilot();

	void Update();

	void SetThrottle(float NewThrottle);
	void UpdateThrottleAdjustments(float Yaw, float Pitch, float Roll);

	void CommitThrottle();

	float GetHeadingHold() { return HeadingHold; }

	template<class NewPolicy> 
	void ChangeFlightPolicy()
	{
		// Construct a new flight controller to load the new flight thread.
		// Elements of old flight controller will be copied to the new one such as the current PID's to preserve state
		try
		{
			Controller.reset();

			Controller = std::shared_ptr<FlightControllerBase>(new FlightController<NewPolicy>());

			std::cout << "Flight mode changing to: " << reinterpret_cast<FlightController<NewPolicy>*>(Controller.get())->GetFlightPolicy().ToString() << std::endl;
		}
		catch(std::exception& e)
		{
			std::cout << "Failed to switch to new flight mode" << std::endl;
			std::terminate();
		}
	}

	std::shared_ptr<FlightControllerBase> GetFlightController()
	{ 
		if(Controller.get() == nullptr)
		{
			std::cout << "Flight controller doesn't exist" << std::endl;
			std::terminate();
		}

		return Controller;
	}

private:
	std::shared_ptr<FlightControllerBase> Controller;

	float Throttle;
	float HeadingHold;

	std::chrono::time_point<std::chrono::steady_clock> LastUpdate;
};

extern std::atomic<float> AdjustedYawThrottle;
extern std::atomic<float> AdjustedPitchThrottle;
extern std::atomic<float> AdjustedRollThrottle;