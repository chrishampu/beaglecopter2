#include "NetHTTPInterpreter.h"
#include "NetHTTPResponse.h"
#include "NetHTTPRequest.h"
#include "NetPlatformLinux.h"
#include "NetPacketManager.h"
#include "NetWebsocketParser.h"
#include "SHA1.h"
#include "Base64.h"
#include <sstream>
#include <iostream>
#include <fstream>
#include <thread>
#include <random>

#include <netinet/in.h>

NetHTTPInterpreter::NetHTTPInterpreter(NetPacket *packet, NetSession *session)
{
	httpPacket = packet;
	httpSession = session;
}

NetHTTPInterpreter::~NetHTTPInterpreter()
{

}

void NetHTTPInterpreter::InterpretPacket()
{
	const char *data; // This holds static data
	std::stringstream buffer; // This is an intermediary buffer that can easily be written to, unlike a string

	// TODO: Why is stringstream even used here? Can convert buffer directly
	buffer << httpPacket->PacketData.getBuffer();
	std::string stringdata = buffer.str();
	data = stringdata.c_str();

	unsigned int dataStart = 2;
	unsigned int payloadSize = 0;

	int opcode = (int)data[0] & 0x0F;
	unsigned char fin = (data[0] >> 7) & 0x01;
	unsigned char masked = (bool)((data[1] >> 7) & 0x01);

	unsigned int length = (unsigned char)(data[1] & (~0x80));

	if (length <= 125) {
		payloadSize = length;
	}
	else if (length == 126) {
		payloadSize = (unsigned char)(data[2] + (data[3] << 8));
		dataStart += 2;
	}
	else if (length == 127) {
		payloadSize = (unsigned char)(data[2] + (data[3] << 8));
		dataStart += 8;
	}

	// This becomes our writable buffer, for unmasking purposes
	char *payload = new char[payloadSize + 1]; // +1 to make room for null termination

	if (masked)
	{
		unsigned int mask = *((unsigned int*)(data + dataStart));
		dataStart += 4;

		memcpy(payload, data + dataStart, payloadSize);

		char* c = payload;

		for (unsigned int i = 0; i < payloadSize; i++)
			c[i] = c[i] ^ ((unsigned char*)(&mask))[i % 4];
	}
	else
	{
		memcpy(payload, data + dataStart, payloadSize);
	}

	payload[payloadSize] = 0; // Null terminate!

	switch (opcode)
	{
		case TextFrame:
		{
			NetWebsocketParser::Instance()->ParseJson(payload);
		}
		break;

		case BinaryFrame:
		{
			printf("Websocket binary message received\n");

			/** This is the server parsing the received binary packet.
				The packets payload gets rewritten with the parsed payload + size,
				and then processed using our known packet structures.
				**/
			/*NetPacket *packet = new NetPacket;

			packet->PacketData.write(payload, payloadSize);

			// Re-process the packet using TCP-style buffers
			NetPacketManager::Instance()->processPacketReceivedEvent(packet, httpSession);

			delete packet;*/
		}
		break;

		case CloseFrame:
		{
			

			// According to RFC6455, the server MUST reply to close frames as quickly as possible
			// The first 2 bytes received are, in network byte order, the code used to describe why the close frame
			// was sent. If there are more bytes after the first 2, then the following bytes describe the 'reason'
			// When replying, it is customary to reply with the error code that was received, if there was one
			
			NetPacket *packet = new NetPacket();

			packet->setPacketSize(0);

			if (payloadSize >= 2)
			{
				union {
					unsigned short i;
					char c[2];
				} val;

				val.c[0] = payload[0];
				val.c[1] = payload[1];

				unsigned short code = ntohs(val.i);

				printf("Websocket close frame: %s\n", ErrorCodeToString((FrameErrorCode)code).c_str());

				packet->PacketData << val.i;
				packet->setPacketSize(2);

				if (payloadSize > 2)
				{
					std::stringstream sbuffer;
					sbuffer << payload;
				}
			}

			EncodeAsFrame(packet, CloseFrame);


			NetPlatformLinux::Instance()->sendtoSocket(httpSession, packet->getPacketBuffer(), packet->getPacketSize());

			// Signal that this connection needs to shut down
			httpSession->ConnectionState = ConnectionStateType::Closed;

			printf("Closing websocket connection\n");

			delete packet;	
		}
		break;

		case PingFrame:
			printf("Websocket ping frame\n");
			break;
		case PongFrame:
		printf("Websocket pong frame\n");
			break;
	}

	delete[] payload;
}

void NetHTTPInterpreter::AuthenticateHandshake()
{
	HTTPRequestBuffer req(reinterpret_cast<const char*>(httpPacket->getPacketBuffer()));

	std::string accept = CalcAcceptKey(req.GetHeader("Sec-WebSocket-Key"));

	HTTPResponseBuffer<HTTP101> response;

	response.SetHeader("Upgrade", "websocket");
	response.SetHeader("Connection", "Upgrade");
	response.SetHeader("Sec-WebSocket-Accept", accept);

	std::string buf = response.GetResponseBuffer();

	if (NetPlatformLinux::Instance()->sendtoSocket(httpSession, reinterpret_cast<const unsigned char*>(buf.c_str()), buf.length()) == NetPlatformBase::Success)
		httpSession->WebSocketHandshakeStatus = HandshakeComplete;
}

std::string NetHTTPInterpreter::CalcAcceptKey(std::string key)
{
	unsigned char hash[40] = { 0 };

	key.append("258EAFA5-E914-47DA-95CA-C5AB0DC85B11"); // Magic string

	Crypto::CalcHashSHA(key.data(), key.size(), hash);

	return Crypto::Base64Encode(reinterpret_cast<const unsigned char*>(hash), 20); // Hash should be of length 20
}

bool NetHTTPInterpreter::DetectWebSocketUpgradeRequest(NetPacket *packet)
{
	HTTPRequestBuffer req(reinterpret_cast<const char*>(packet->getPacketBuffer()));

	if (req.GetHeader("Upgrade").compare("websocket") == 0)
		return true;

	return false;
}

void NetHTTPInterpreter::SendWebSocketUpgradeRequest(NetSession *session)
{
	std::stringstream buffer;
	std::default_random_engine generator;
	std::uniform_int_distribution<char> distribution(0,9);

	char hash[16] = {0};

	for(char i = 0; i < 16; i++)
		hash[i] = distribution(generator);

	buffer << "GET /socket/websocket HTTP/1.1\r\n"
		   << "Host: " << session->getAddress().getIPString() << "\r\n"
		   << "Upgrade: websocket\r\n"
		   << "Connection: Upgrade\r\n"
		   << "Sec-WebSocket-Key: " << Crypto::Base64Encode(reinterpret_cast<const unsigned char*>(hash), 16) << "\r\n"
		   << "Sec-WebSocket-Version: 13\r\n"
		   << "Origin: 192.168.0.17\r\n\r\n"; // TODO: Dynamic origin based on current session IP

	NetPlatformLinux::Instance()->sendtoSocket(session, (unsigned char*)buffer.str().c_str(), buffer.str().size());
}

void NetHTTPInterpreter::HandleSocketUpgradeResponse(NetPacket *packet, NetSession *session)
{
	//std::printf("Websocket response: %s\n", packet->getPacketBuffer());

	// Assume success since we don't yet parse the actual response
	printf("Websocket connected\n");
}

bool NetHTTPInterpreter::HandleGetRequest(NetPacket *packet, NetSession *session)
{
	HTTPRequestBuffer req(reinterpret_cast<const char*>(packet->getPacketBuffer()));

	std::string url = req.Url();

	if (url.compare("/") == 0) // Only a WebSocket handshake request should be asking for the root
	{
		std::string content = "UVic AERO Datacenter";

		HTTPResponseBuffer<HTTP200> response;

		response.SetHeader("Content-Type", "text/html");
		response.SetHeader("Content-Length", std::to_string(content.length()));
		response.SetContent(content);

		std::string buf = response.GetResponseBuffer();

		NetPlatformLinux::Instance()->sendtoSocket(session, (unsigned char*)buf.c_str(), buf.size());
	}
	else if (url.find(".jpg") != std::string::npos)
	{
		// Client is requesting a file on this server
		std::string target = DocumentRoot;

		target.append("/images");

		target.append(url);

		// Thread the actual file loading so it doesn't block the main thread, as file loading can be slow
		std::thread FileThread(&NetHTTPInterpreter::SendFileToSession, target, std::ref(session));
		FileThread.detach();
	}
	else
	{
		HTTPResponseBuffer<HTTP404> response;
		response.SetHeader("Content-Type", "text/html");
		response.SetHeader("Content-Length", std::to_string(9));
		response.SetContent("Not found");

		std::string buf = response.GetResponseBuffer();

		NetPlatformLinux::Instance()->sendtoSocket(session, (unsigned char*)buf.c_str(), buf.size());

		return false;
	}

	return true;
}

void NetHTTPInterpreter::SendFileToSession(std::string path, NetSession* session)
{
	std::ifstream file(path, std::ios::in | std::ios::binary | std::ios::ate);

	if (file.is_open())
	{
		unsigned int size = (unsigned int)file.tellg();
		char *data = new char[size];

		file.seekg(0, std::ios::beg);
		file.read(data, size);

		file.close();

		HTTPResponseBuffer<HTTP200> response;

		if (path.find(".jpg") != std::string::npos)
			response.SetHeader("Content-Type", "image/jpg");
		else
			response.SetHeader("Content-Type", "pdf");

		response.SetHeader("Content-Length", std::to_string(size));
		response.SetContent(data);

		std::string buf = response.GetResponseBuffer();

		NetPlatformLinux::Instance()->sendtoSocket(session, (unsigned char*)buf.c_str(), buf.size());
	}
	else
	{
		HTTPResponseBuffer<HTTP404> response;
		response.SetHeader("Content-Type", "text/html");
		response.SetHeader("Content-Length", std::to_string(9));
		response.SetContent("Not found");

		std::string buf = response.GetResponseBuffer();

		NetPlatformLinux::Instance()->sendtoSocket(session, (unsigned char*)buf.c_str(), buf.size());
	}
}

void NetHTTPInterpreter::EncodeAsFrame(NetPacket *packet, WebSocketOpcode opcode)
{
	// Masking only needs to be done for client -> server messages, so change this
	// in the future to handle server -> client messages without masking

	const bool masked = true;
	unsigned char maskingKey[4];
	const unsigned char *buffer = packet->getPacketBuffer();
	unsigned char header[16];
	int pos = 0;
	unsigned int size = packet->getPacketSize();

	// Generate masking key
	if(masked)
	{
		std::default_random_engine generator;
		std::uniform_int_distribution<char> distribution(0,9);

		for(int i = 0; i < 4; i++)
			maskingKey[i] = distribution(generator);
	}

	header[pos++] = (unsigned char)(0x80 + opcode);

	if (size <= 125)
	{
		if(masked)
			header[pos++] = (size | (1 << 7));
		else
			header[pos++] = size;
	}
	else if (size <= 65535)
	{
		printf("Large packet\n");
		if(masked)
			header[pos++] = (126 | (1 << 7));
		else
			header[pos++] = 126;

		header[pos++] = (size >> 8) & 0xFF;
		header[pos++] = size & 0xFF;
	}
	else
	{
		int64_t dsize = size;

		if(masked)
			header[pos++] = (127 | (1 << 7));
		else
			header[pos++] = 127;

		header[pos++] = (dsize >> 56) & 0xFF;
		header[pos++] = (dsize >> 48) & 0xFF;
		header[pos++] = (dsize >> 40) & 0xFF;
		header[pos++] = (dsize >> 32) & 0xFF;
		header[pos++] = (size >> 24) & 0xFF;
		header[pos++] = (size >> 16) & 0xFF;
		header[pos++] = (size >> 8) & 0xFF;
		header[pos++] = size & 0xFF;
	}

	unsigned int packetSize = pos + size + (masked ? 4 : 0);
	unsigned char *data = new unsigned char[packetSize];
	memcpy(data, header, pos);

	if(masked)
	{
		memcpy(data + pos, maskingKey, sizeof(char) * 4);
		memcpy(data + pos + 4, buffer, size);

		unsigned char *maskedBuffer = (unsigned char*)(data) + pos + 4;

		// Perform masking of buffer
		for (unsigned int i = 0; i < size; i++)
			maskedBuffer[i] = maskedBuffer[i] ^ maskingKey[i % 4];
	}
	else
		memcpy(data + pos, buffer, size);

	packet->PacketData.clear();

	packet->PacketData.write(data, packetSize);
	packet->setPacketSize(packetSize);

	delete[] data;
}

bool NetHTTPInterpreter::isHTTPPacket(NetPacket *packet)
{
	std::string buf = reinterpret_cast<const char*>(packet->getPacketBuffer());

	if (buf.length() >= 3)
		if (buf.substr(0, 3) == "GET")
			return true;
	return false;
}

std::string NetHTTPInterpreter::ErrorCodeToString(FrameErrorCode code)
{
	switch (code)
	{
	case NormalClosure:
		return "Normal closure";

	case GoingAway:
		return "Browser navigated away";

	case ProtocolError:
		return "Protocol error";

	case UnacceptableData:
		return "Unacceptable data";

	case Reserved:
	case Reserved2:
	case Reserved3:
		return "Reserved error";

	case InconsistentData:
		return "Inconsistent data";

	case PolicyViolation:
		return "Policy violation";

	case OversizedMessage:
		return "Oversized message";

	case ExtensionNegotiationFail:
		return "Negotiation failure";

	case UnexpectedCondition:
		return "Unexpected condition";

	default:
		return "Unknown error code";
	}
}