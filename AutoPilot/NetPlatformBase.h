#pragma once

#include "NetSocket.h"
#include "NetSession.h"

// Forward declarations
struct NetAddress;
class NetSession;

// Base class for multi platform networking
class NetPlatformBase
{
public:
	NetPlatformBase()
		: Initialized(false) { }

	virtual ~NetPlatformBase() { }

	enum NetError
	{
		Success,
		WrongProtocolType,
		InvalidPacketProtocol,
		WouldBlock,
		NotASocket,
		ConnectionInProgress,
		ConnectionRefused,
		ConnectionTimedOut,
		ConnectionReset,
		BadAddress,
		NotConnected,
		UnknownError
	};

	enum ConnectionState
	{
		DNSResolved,
		DNSFailed,
		Connected,
		ConnectFailed,
		Disconnected
	};

	enum Protocol
	{
		UDP,
		TCP
	};

	// Runtime interface
	virtual bool Init() = 0;
	virtual void Shutdown() = 0;
	virtual void Process() = 0;

	// Core high level interface
	virtual NetSocket openListenPort(const char *address = "", unsigned short port = 0, bool useSSL = false) = 0;
	virtual NetSocket openConnectTo(char *stringAddress, unsigned short port = 0, NetSessionType RequestedSessionType = NetSessionType::Undetermined, bool Reconnect = false) = 0;
	virtual void closeConnectTo(NetSocket socket) = 0;
	virtual NetError sendtoSocket(NetSession *session, const unsigned char *buffer, int bufferSize) = 0;
	
	// Utility functions
	virtual NetError getLastError() = 0;
	virtual std::string NetErrorToString(const NetError error) = 0;
	virtual std::string NetAddressToString(const NetAddress *address) = 0;

protected:
	// Setters
	virtual NetError setBufferSize(NetSocket socket, int bufferSize) = 0;
	virtual NetError setBroadcast(NetSocket socket, bool broadcastEnable) = 0;
	virtual NetError setBlocking(NetSocket socket, bool blockingIO) = 0;

	// Private utility functions
	virtual NetSocket openSocket() = 0;
	virtual NetError closeSocket(NetSocket socket) = 0;

	// Core low level functions
	virtual NetError send(NetSession *session, const char *buffer, int bufferSize) = 0;
	virtual NetError recv(NetSession *session, char *buffer, int bufferSize, int *bytesRead) = 0;

	virtual NetError connect(NetSocket socket, const NetAddress *address) = 0;
	virtual NetError listen(NetSocket socket, int maxConcurrentListens) = 0;
	virtual NetSocket accept(NetSocket acceptSocket, NetAddress *remoteAddress) = 0;
	virtual NetError bind(NetSocket socket, unsigned short port, const char *address = "") = 0;

protected:
	bool Initialized;

	std::vector<NetSocketObject*> ListenSockets;

public:
	NetSocketObject *getObjectBySocket(NetSocket socket);
};