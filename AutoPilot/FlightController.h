#pragma once

#include "FlightPolicies.h"
#include <thread>
#include <iostream>
#include <functional>
#include <exception>
#include <system_error>
#include <atomic>

class FlightControllerBase
{
public:
	virtual const Flight& GetFlightPolicy() const = 0;
	virtual const void SignalTuningParametersChanged() const = 0;
};

// Generic class to control flight based on current mode
template<class FlightPolicy>
class FlightController : public FlightControllerBase
{
public:
 	FlightController(const FlightPolicy& Mode = FlightPolicy())
 		: _Flight(Mode)
	 {
	 	try
	 	{
		    sched_param sch;
		    int policy; 

		    // Create new thread
			std::thread FThread(&FlightPolicy::FlightLoop, &_Flight);

			// Save the thread
			FlightLoop.swap(FThread);
		 
		 	// Now set priority on this thread very high for maximum consistency
		    pthread_getschedparam(FlightLoop.native_handle(), &policy, &sch);
		    sch.sched_priority = 99;

		    if(pthread_setschedparam(FlightLoop.native_handle(), SCHED_FIFO, &sch))
		    	throw std::system_error(EPERM, std::system_category());
	 	}
	 	catch(std::exception& e)
	 	{
	 		std::terminate();
	 	}
	}

    FlightController(FlightController&& rhs) = delete;
    
	~FlightController()
	{
		_Flight.ContinueFlightMode.store(false);

		if(FlightLoop.joinable())
			FlightLoop.join();
	}

	const void SignalTuningParametersChanged()
	{
		_Flight.TuningParametersChanged.store(true);
	}

	const Flight& GetFlightPolicy() const { return static_cast<const Flight&>(_Flight); }

private:
	FlightPolicy _Flight;
	std::thread FlightLoop;
};