#include "PID.h"
#include <iostream>

const float ITermLimit = 8.0f;
const float OutputLimit = 800.0f;

std::atomic<float> TuningPTerm;
std::atomic<float> TuningITerm;
std::atomic<float> TuningDTerm;

PID::PID(float tP, float tI, float tD)
	: PTerm(1.f)
	, ITerm(1.f)
	, DTerm(1.f)
	, TunePTerm(tP)
	, TuneITerm(tI)
	, TuneDTerm(tD)
	, DeltaTime(0.f)
	, FirstLoop(true)
	, LastInput(0.f)
{
	TuneParameters(DefaultDeltaTime);
}

PID::~PID()
{
}

void PID::TuneParameters(float FixedDeltaTime)
{
	TuneParameters(FixedDeltaTime, TunePTerm, TuneITerm, TuneDTerm);
}

void PID::TuneParameters(float tP, float tI, float tD)
{
	TuneParameters(DefaultDeltaTime, tP, tI, tD);
}

void PID::TuneParameters(float FixedDeltaTime, float tP, float tI, float tD)
{
	DeltaTime = FixedDeltaTime;

	TunePTerm = tP;
	TuneITerm = tI;
	TuneDTerm = tD;

	TimedPTerm = TunePTerm;
	TimedITerm = TuneITerm * (DeltaTime / 1000.0f);
	TimedDTerm = TuneDTerm / (DeltaTime / 1000.0f);

	//std::cout << "TP: " << TimedPTerm << " TI: " << TimedITerm << " TD: " << TimedDTerm << std::endl;
}

float PID::Calculate(float Target, float Current)
{
	float Output = 0;
	float Error = Target - Current;

	// P Term
	PTerm = TimedPTerm * Error;

	// I Term
	ITerm = (ITerm + (TimedITerm * Error));

	if(ITerm > ITermLimit)
		ITerm = ITermLimit;
	else if(ITerm < -ITermLimit)
		ITerm = -ITermLimit;

	// D Term
	if(FirstLoop == false)
	{
		DTerm = -TimedDTerm * (Current - LastInput);
		//std::cout << "D = " << DTerm << " TimedD = " << TimedDTerm << " C = " << Current << " L = " << LastInput << std::endl;
	}
	else
	{
		DTerm = 0.0f;
		FirstLoop = false;
	}

	// PID
	Output = PTerm + ITerm + DTerm;

	if(Output > OutputLimit)
		Output = OutputLimit;
	else if(Output < -OutputLimit)
		Output = OutputLimit;

	LastInput = Current;

	//std::cout << "P: " << PTerm << " I: " << ITerm << " D: " << DTerm << std::endl;

	return Output;
}