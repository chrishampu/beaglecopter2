#pragma once

#include <algorithm>
#include <map>

template<class HTTPMessage>
class HTTPBuffer
{
private:
	// Self type
	typedef HTTPBuffer<HTTPMessage> _MyBuffer;

public:
	// Type of template class
	typedef HTTPMessage http_type;

	// Basic types
	typedef int http_code_type;
	typedef std::string http_string_type;

	// Buffer types
	typedef std::map<http_string_type, http_string_type> buffer_type;
	buffer_type _Buffer;

	// Iterator types
	typedef typename buffer_type::iterator http_iterator;
	typedef typename http_string_type::iterator http_string_iterator;

public:
	HTTPBuffer()
	{

	}

	// Copy constructor
	HTTPBuffer(const _MyBuffer& right)
	{
		Copy(right);
	}

	// Move constructor
	HTTPBuffer(_MyBuffer&& right)
	{
		Swap( std::forward<_MyBuffer>(right));
	}

	// Copy operator
	HTTPBuffer& operator=(_MyBuffer& right)
	{
		if (this != &right)
		{
			Clear();
			Copy(right);
		}

		return (*this);
	}

	// Move operator
	HTTPBuffer& operator=(_MyBuffer&& right)
	{
		Swap(std::forward<_MyBuffer>(right));

		return (*this);
	}

	http_string_type GetHeader(http_string_type key)
	{
		http_iterator iter = FindHeader(key);

		return iter == _Buffer.end() ? http_string_type() : _Buffer[key];
	}

	http_iterator FindHeader(http_string_type key)
	{
		return _Buffer.find(key);
	}

	_MyBuffer& SetHeader(http_string_type key, http_string_type val)
	{
		_Buffer[key] = val;

		return (*this);
	}

	void Clear()
	{
		_Buffer.clear();
	}

protected:
	http_string_iterator SearchHeader(http_string_iterator begin, http_string_iterator end, http_string_type delim)
	{
		// Hacking at its finest..
		return std::search(
			begin,
			end,
			delim.begin(),
			delim.end()
			);
	}

	void Copy(const _MyBuffer& right)
	{
		_Buffer = right._Buffer;
	}

	void Swap(_MyBuffer&& right)
	{
		std::swap(_Buffer, right._Buffer);
	}
};