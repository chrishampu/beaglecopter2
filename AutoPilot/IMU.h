#pragma once

#include "Instanced.h"
#include "Vector.h"
#include <mutex>
#include <atomic>
#include <chrono>

class IMU : public Instanced<IMU>
{
public:
	IMU();
	~IMU();

	void Calibrate();
	void Poll();

	Vector GetOrientation();
	void SetOrientation(Vector NewOrientation);

private:
	void ProcessSensorString(std::string Line);

	Vector Orientation;

	float Temperature;
	float Altitude;

	int serialHandle;

	std::chrono::time_point<std::chrono::steady_clock> LastUpdate;
};