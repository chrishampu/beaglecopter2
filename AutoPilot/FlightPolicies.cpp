#include "FlightPolicies.h"
#include "MotorController.h"
#include "AutoPilot.h"
#include "IMU.h"
#include <iostream>
#include <chrono>
#include <thread>
#include <iomanip>
#include <tgmath.h>

// Default flight
Flight::Flight()
	: YawPID(TuningPTerm, TuningITerm, TuningDTerm)
	, PitchPID(TuningPTerm, TuningITerm, TuningDTerm)
	, RollPID(TuningPTerm, TuningITerm, TuningDTerm)
	, ContinueFlightMode(true)
	, TuningParametersChanged(false)
{
}

// TODO: Transfer tuning PID terms between flight mode changes
Flight::Flight(const Flight& Other)
	: YawPID(TuningPTerm, TuningITerm, TuningDTerm)
	, PitchPID(TuningPTerm, TuningITerm, TuningDTerm)
	, RollPID(TuningPTerm, TuningITerm, TuningDTerm)
	, ContinueFlightMode(true)
	, TuningParametersChanged(false)
{
}

Flight::~Flight()
{
}

// Grounded flight
GroundedFlight::GroundedFlight()
{
	// If we're initiating grounded mode, then disable motors
	MotorController::Throttle<All>::Set(0);
	MotorController::Instance()->StopMotors();

	std::cout << "Initializing grounded flight mode" << std::endl;
}

GroundedFlight::~GroundedFlight()
{
	// If we're taking off then start motors again
	MotorController::Instance()->StartMotors();
}

void GroundedFlight::FlightLoop()
{
	std::cout << "Entered grounded flight loop" << std::endl;

	while(ContinueFlightMode.load() == true)
	{
		; // Do nothing
	}

	std::cout << "Exiting grounded flight loop" << std::endl;
}

// Stabilized flight
StabilizedFlight::StabilizedFlight()
{
	std::cout << "Initializing stabilized flight mode" << std::endl;
}

StabilizedFlight::~StabilizedFlight()
{

}

void StabilizedFlight::FlightLoop()
{
	AutoPilot *Pilot = AutoPilot::Instance();
	IMU * Imu = IMU::Instance();

	std::cout << "Entered stabilized flight loop" << std::endl;

	float HeadingHold = IMU::Instance()->GetOrientation().z;

	if(HeadingHold == 0.0f)
		std::cout << "Warning: Starting yaw is 0 which may be inaccurate or indicate IMU did not start correctly" << std::endl;

	printf("Heading hold set to %.2f\n", HeadingHold);

	auto Last = std::chrono::steady_clock::now();

	//auto Benchmark = std::chrono::steady_clock::now();
	//auto bticks = std::chrono::duration_cast<std::chrono::seconds>(std::chrono::seconds(1)).count();
	//int LoopCount = 0;

	auto Ticks = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::duration<double, std::milli>(DefaultDeltaTime)).count();

	while(ContinueFlightMode.load() == true)
	{
		auto Now = std::chrono::steady_clock::now();

		auto CurrentCount = std::chrono::duration_cast<std::chrono::microseconds>(Now - Last);

		if(CurrentCount.count() >= Ticks)
		{
			//LoopCount++;

			Vector Position = Imu->GetOrientation();

			// Normalize the roll to 0
			float AdjustedRoll = 0.0f;

			if(Position.x > 0)
				AdjustedRoll = fabs(Position.x - 180.0f); 
			else
				AdjustedRoll = -fabs(Position.x + 180.0f);

			 // x is roll, y is pitch, z is yaw
			float Yaw = YawPID.Calculate(HeadingHold, Position.z);
			float Pitch = PitchPID.Calculate(0, Position.y);
			float Roll = RollPID.Calculate(0, AdjustedRoll);

			//std::cout << std::fixed << std::setprecision(2) << "A Y: " << Position.x << " P: " << Position.y << " R: " << Position.z << std::endl;

			Pilot->UpdateThrottleAdjustments(Yaw, Pitch, Roll);
			
			
			//std::cout << std::fixed << std::setprecision(2) << "C Y: " << Yaw << " P: " << Pitch << " R: " << Roll << std::endl;

			Last = Now;
		}
		else
		{
			if(TuningParametersChanged.load() == true)
			{
				YawPID.TuneParameters(TuningPTerm.load(), TuningITerm.load(), TuningDTerm.load());
				PitchPID.TuneParameters(TuningPTerm.load(), TuningITerm.load(), TuningDTerm.load());
				RollPID.TuneParameters(TuningPTerm.load(), TuningITerm.load(), TuningDTerm.load());

				TuningParametersChanged.store(false);
			}

			std::this_thread::sleep_for(std::chrono::duration<double, std::milli>(1));
		}

		/*
		if(std::chrono::duration_cast<std::chrono::seconds>(Now - Benchmark).count() >= bticks)
		{
			std::cout << LoopCount << " loops per second" << std::endl;
			LoopCount = 0;
			Benchmark = Now;
		}
		*/
	}

	std::cout << "Exiting stabilized flight loop" << std::endl;
}

 // Landing flight
LandingFlight::LandingFlight()
{
	std::cout << "Initializing landing flight mode" << std::endl;
}

LandingFlight::~LandingFlight()
{

}

void LandingFlight::FlightLoop()
{
	std::cout << "Entered landing flight loop" << std::endl;	

	while(ContinueFlightMode.load() == true)
	{
		; // Do nothing
	}

	std::cout << "Exiting landing flight loop" << std::endl;
}