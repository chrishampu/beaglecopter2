#pragma once

#include "NetPlatformBase.h"
#include "Instanced.h"

class NetPlatformLinux : public NetPlatformBase, public Instanced<NetPlatformLinux>
{
public:
	NetPlatformLinux() { }
	~NetPlatformLinux() { }

	virtual bool Init();
	virtual void Shutdown();

	virtual void Process();

	// TCP functions
	virtual NetSocket openListenPort(const char *address = "", unsigned short port = 0, bool useSSL = false);
	virtual NetSocket openConnectTo(char *stringAddress, unsigned short port = 0, NetSessionType RequestedSessionType = NetSessionType::Undetermined, bool Reconnect = false);
	virtual void closeConnectTo(NetSocket socket);
	virtual NetError sendtoSocket(NetSession *session, const unsigned char *buffer, int bufferSize);

	virtual NetSocket openSocket();
	virtual  NetError closeSocket(NetSocket socket);

	virtual  NetError getLastError();
	virtual std::string NetErrorToString(const NetError error);
	virtual  std::string NetAddressToString(const NetAddress *address);

	// Lower level functions
	virtual  NetError send(NetSession *session, const char *buffer, int bufferSize);
	virtual  NetError recv(NetSession *session, char *buffer, int bufferSize, int *bytesRead);

	virtual  NetError connect(NetSocket socket, const NetAddress *address);
	virtual NetError listen(NetSocket socket, int maxConcurrentListens);
	virtual NetSocket accept(NetSocket acceptSocket, NetAddress *remoteAddress);

	virtual NetError detectLocalAddress(char *address);
	virtual NetError bind(NetSocket socket, unsigned short port, const char *address = "");
	virtual NetError setBufferSize(NetSocket socket, int bufferSize);
	virtual NetError setBroadcast(NetSocket socket, bool broadcastEnable);
	virtual NetError setBlocking(NetSocket socket, bool blockingIO);
};