#pragma once

#include "Instanced.h"
#include "Motor.h"
#include <map>
#include <chrono>

enum MotorSelector
{
	All = 10,
	CW,
	CCW
};

class MotorController : public Instanced<MotorController>
{
public:
	MotorController();
	~MotorController();

	void Update();

	void ArmMotors();

	void StopMotors();
	void StartMotors();

	template<int T>
	struct Throttle {

		// Inc/dec by delta value
		static void Increase(unsigned int Value);
		static void Decrease(unsigned int Value);

		// Set to specific value
		static void Set(unsigned int Value);
	};

protected:
	typedef std::map<MotorDirection, Motor*> MotorMapT;
	MotorMapT Motors;

	std::chrono::time_point<std::chrono::steady_clock> LastUpdate;
};

// Increase specialization
template<> inline void MotorController::Throttle<MotorDirection::North>::Increase(unsigned int Value)
{
	MotorController::Instance()->Motors[MotorDirection::North]->SetThrottle( MotorController::Instance()->Motors[MotorDirection::North]->GetThrottle() + Value );
}

template<> inline void MotorController::Throttle<MotorDirection::South>::Increase(unsigned int Value)
{
	MotorController::Instance()->Motors[MotorDirection::South]->SetThrottle( MotorController::Instance()->Motors[MotorDirection::South]->GetThrottle() + Value );
}

template<> inline void MotorController::Throttle<MotorDirection::West>::Increase(unsigned int Value)
{
	MotorController::Instance()->Motors[MotorDirection::West]->SetThrottle( MotorController::Instance()->Motors[MotorDirection::West]->GetThrottle() + Value );
}

template<> inline void MotorController::Throttle<MotorDirection::East>::Increase(unsigned int Value)
{
	MotorController::Instance()->Motors[MotorDirection::East]->SetThrottle( MotorController::Instance()->Motors[MotorDirection::East]->GetThrottle() + Value );
}

template<> inline void MotorController::Throttle<MotorSelector::All>::Increase(unsigned int Value)
{
	for ( const auto& motor : MotorController::Instance()->Motors )
	{
		motor.second->SetThrottle( motor.second->GetThrottle() + Value );
	}
}

template<> inline void MotorController::Throttle<MotorSelector::CW>::Increase(unsigned int Value)
{
	MotorController::Instance()->Motors[MotorDirection::North]->SetThrottle( MotorController::Instance()->Motors[MotorDirection::North]->GetThrottle() + Value );
	MotorController::Instance()->Motors[MotorDirection::South]->SetThrottle( MotorController::Instance()->Motors[MotorDirection::South]->GetThrottle() + Value );
}

template<> inline void MotorController::Throttle<MotorSelector::CCW>::Increase(unsigned int Value)
{
	MotorController::Instance()->Motors[MotorDirection::East]->SetThrottle( MotorController::Instance()->Motors[MotorDirection::East]->GetThrottle() + Value );
	MotorController::Instance()->Motors[MotorDirection::West]->SetThrottle( MotorController::Instance()->Motors[MotorDirection::West]->GetThrottle() + Value );
}

// Decrease specialization
template<> inline void MotorController::Throttle<MotorDirection::North>::Decrease(unsigned int Value)
{
	MotorController::Instance()->Motors[MotorDirection::North]->SetThrottle( MotorController::Instance()->Motors[MotorDirection::North]->GetThrottle() - Value );
}

template<> inline void MotorController::Throttle<MotorDirection::South>::Decrease(unsigned int Value)
{
	MotorController::Instance()->Motors[MotorDirection::South]->SetThrottle( MotorController::Instance()->Motors[MotorDirection::South]->GetThrottle() - Value );
}

template<> inline void MotorController::Throttle<MotorDirection::West>::Decrease(unsigned int Value)
{
	MotorController::Instance()->Motors[MotorDirection::West]->SetThrottle( MotorController::Instance()->Motors[MotorDirection::West]->GetThrottle() - Value );
}

template<> inline void MotorController::Throttle<MotorDirection::East>::Decrease(unsigned int Value)
{
	MotorController::Instance()->Motors[MotorDirection::East]->SetThrottle( MotorController::Instance()->Motors[MotorDirection::East]->GetThrottle() - Value );
}

template<> inline void MotorController::Throttle<MotorSelector::All>::Decrease(unsigned int Value)
{
	for ( const auto& motor : MotorController::Instance()->Motors )
	{
		motor.second->SetThrottle( motor.second->GetThrottle() - Value );
	}
}

template<> inline void MotorController::Throttle<MotorSelector::CW>::Decrease(unsigned int Value)
{
	MotorController::Instance()->Motors[MotorDirection::North]->SetThrottle( MotorController::Instance()->Motors[MotorDirection::North]->GetThrottle() - Value );
	MotorController::Instance()->Motors[MotorDirection::South]->SetThrottle( MotorController::Instance()->Motors[MotorDirection::South]->GetThrottle() - Value );
}

template<> inline void MotorController::Throttle<MotorSelector::CCW>::Decrease(unsigned int Value)
{
	MotorController::Instance()->Motors[MotorDirection::East]->SetThrottle( MotorController::Instance()->Motors[MotorDirection::East]->GetThrottle() - Value );
	MotorController::Instance()->Motors[MotorDirection::West]->SetThrottle( MotorController::Instance()->Motors[MotorDirection::West]->GetThrottle() - Value );
}

// Set Specialization
template<> inline void MotorController::Throttle<MotorDirection::North>::Set(unsigned int Value)
{
	MotorController::Instance()->Motors[MotorDirection::North]->SetThrottle( Value );
}

template<> inline void MotorController::Throttle<MotorDirection::South>::Set(unsigned int Value)
{
	MotorController::Instance()->Motors[MotorDirection::South]->SetThrottle( Value );
}

template<> inline void MotorController::Throttle<MotorDirection::West>::Set(unsigned int Value)
{
	MotorController::Instance()->Motors[MotorDirection::West]->SetThrottle( Value );
}

template<> inline void MotorController::Throttle<MotorDirection::East>::Set(unsigned int Value)
{
	MotorController::Instance()->Motors[MotorDirection::East]->SetThrottle( Value );
}

template<> inline void MotorController::Throttle<MotorSelector::All>::Set(unsigned int Value)
{
	for ( const auto& motor : MotorController::Instance()->Motors )
	{
		motor.second->SetThrottle( Value );
	}
}

template<> inline void MotorController::Throttle<MotorSelector::CW>::Set(unsigned int Value)
{
	MotorController::Instance()->Motors[MotorDirection::North]->SetThrottle( Value );
	MotorController::Instance()->Motors[MotorDirection::South]->SetThrottle( Value );
}

template<> inline void MotorController::Throttle<MotorSelector::CCW>::Set(unsigned int Value)
{
	MotorController::Instance()->Motors[MotorDirection::East]->SetThrottle( Value );
	MotorController::Instance()->Motors[MotorDirection::West]->SetThrottle( Value );
}