#pragma once

#include "Instanced.h"
#include "Vector.h"
#include <string>

class NetWebsocketParser : public Instanced<NetWebsocketParser>
{
public:
	NetWebsocketParser();
	~NetWebsocketParser();

	void ParseJson(std::string payload);

	void SendTelemetry(Vector Orientation, float Temperature, float Altitude);
	void SendMotorThrottles(float MotorN, float MotorE, float MotorS, float MotorW);
	void SendPID(float Yaw, float Pitch, float Roll);

	void OnConnectionSuccess();

private:
	void SendJson(std::string json);
};