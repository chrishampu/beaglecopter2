#include "NetPlatformLinux.h"
#include "NetSession.h"
#include "NetPacketManager.h"
#include "NetHTTPInterpreter.h"
#include "NetWebsocketParser.h"
#include <string>
#include <memory>

NetSession::NetSession(NetSocket sock, NetAddress addr)
	: Socket(sock),
	Address(addr),
	SocketType(Undetermined),
	ConnectionState(InvalidState),
	HasSSL(false),
	SSLHandshakeStatus(Incomplete),
	WebSocketHandshakeStatus(HandshakeIncomplete),
	ShouldReconnect(false),
	LastConnectionAttempt(std::chrono::steady_clock::now()),
	ConnectionAttempts(0)
{
	SessionState.SessionFlags.reset();
	SessionState.parent = this;
}

NetSession::~NetSession()
{
	while (!PacketQueue.empty())
	{
		NetPacket* packet = PacketQueue.front();

		delete packet;

		PacketQueue.pop();
	}

	NetPlatformLinux::Instance()->closeConnectTo(Socket);
}

bool NetSession::Update()
{
	//printf("Type: %d Port: %d Hanshake: %d", SocketType, PortType, WebSocketHandshakeStatus);
	// Send handshake request if this connection just started
	if(SocketType == WebSocket && PortType == ConnectToPort && WebSocketHandshakeStatus == HandshakeIncomplete)
	{
		NetHTTPInterpreter::SendWebSocketUpgradeRequest(this);
		WebSocketHandshakeStatus = HandshakeRequestSent;
		HandshakeSentTime = std::chrono::steady_clock::now();
		printf("Sent websocket handshake request. Waiting for response\n");
	}

	while (!PacketQueue.empty())
	{
		NetPacket* packet = PacketQueue.front();

		if (SocketType == Undetermined)
		{
			if (NetHTTPInterpreter::isHTTPPacket(packet) == true)
			{
				if (NetHTTPInterpreter::DetectWebSocketUpgradeRequest(packet) == true)
				{
					setSocketType(WebSocket);

					// Start a thread and detach it to do long handshake processing and ensure main loop isn't blocked
					// The AuthenticateHandshake function will delete the packet data when it no longer requires it
					NetHTTPInterpreter *interpreter = new NetHTTPInterpreter(packet, this);
					interpreter->AuthenticateHandshake();

					delete interpreter;
				}
				else
				{
					setSocketType(HTTPSocket);

					NetHTTPInterpreter::HandleGetRequest(packet, this);
				}

				delete packet;

				PacketQueue.pop();
				continue;
			}
			else
				setSocketType(TCPSocket);
		}

		// Expecting an HTTP packet with handshake result
		if (SocketType == WebSocket && WebSocketHandshakeStatus == HandshakeRequestSent)
		{
			NetHTTPInterpreter::HandleSocketUpgradeResponse(packet, this);
			WebSocketHandshakeStatus = HandshakeComplete;
			// At this point we assume we have successfully connected.
			// Now send information required by the web interface at first connection
			NetWebsocketParser::Instance()->OnConnectionSuccess();

		}
		else if (SocketType == WebSocket && WebSocketHandshakeStatus == HandshakeComplete)
		{
			std::unique_ptr<NetHTTPInterpreter> interpreter(new NetHTTPInterpreter(packet, this));
			interpreter->InterpretPacket();
		}
		else if (SocketType == TCPSocket)
			NetPacketManager::Instance()->processPacketReceivedEvent(packet, this);
		else if (SocketType == HTTPSocket)
			NetHTTPInterpreter::HandleGetRequest(packet, this);

		delete packet;

		PacketQueue.pop();
	}

	return true;
}

void NetSession::Reconnect()
{
	ConnectionState = Reconnecting;
	LastConnectionAttempt = std::chrono::steady_clock::now();
	WebSocketHandshakeStatus = HandshakeIncomplete;
}

void NetSession::InvalidateSocket()
{
	Socket = InvalidSocket;
}

void NetSession::AddPacket(NetPacket *packet)
{
	PacketQueue.push(packet);
}

std::string NetSession::getAddressString()
{
	return NetPlatformLinux::Instance()->NetAddressToString(&Address);
}