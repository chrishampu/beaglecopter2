#pragma once

#include "NetPacket.h"

class PacketJSONPayload : public NetPacket
{
public:
	PacketJSONPayload();
	PacketJSONPayload(std::string JSON);

	virtual void PreparePacket();

	virtual void Handle(NetStream& ns, NetSession* sess);

	virtual void Decode(NetStream& ns);

	std::string getJSONString() { return JSON; }

private:
	std::string JSON;
};