#pragma once

template<typename T>
class Instanced	
{
private:
	Instanced(const Instanced&) = delete;
	Instanced(Instanced&&) = delete;

	Instanced& operator=(Instanced) = delete;
	Instanced& operator=(Instanced&&) = delete;

protected:
	Instanced(void) {};
	~Instanced(void) {};

public:
	static inline T* Instance(void)
	{
		static T instance;
		return &instance;
	}
};