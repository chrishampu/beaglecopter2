#pragma once

#include "NetHTTPCode.h"
#include "NetHTTPBuffer.h"

template<typename T>
class HTTPResponseBuffer
	: public HTTPBuffer < T >
{
private:
	// Base types
	typedef HTTPBuffer<T> _MyBuffer;
	typedef HTTPResponseBuffer<T> _MyT;

	typedef std::string http_string_type;

	typedef T http_type;

	// Volatile parameters
	http_string_type _Content;
	http_string_type _ResponseBuffer;

public:
	// Default constructor
	HTTPResponseBuffer()
	{

	}

	// Copy constructor
	HTTPResponseBuffer(const _MyT& right)
		: _MyBuffer(right)
	{
		_Content = right._Content;
	}

	// Move constructor
	HTTPResponseBuffer(_MyT&& right)
		: _MyBuffer(std::move(right))
	{
		std::swap(_Content, right._Content);
	}

	// Copy operator
	HTTPResponseBuffer& operator=(_MyT &right)
	{
		_MyBuffer::operator=(right);
		_Content = right._Content;
		return (*this);
	}

	// Move operator
	HTTPResponseBuffer& operator=(_MyT&& right)
	{
		_MyBuffer::operator=(std::move(right));
		std::swap(_Content, right._Content);
		return (*this);
	}

	_MyT& SetContent(http_string_type content)
	{
		_Content = content;

		return (*this);
	}

	const http_string_type GetContent()
	{
		return _Content;
	}

	// Inline functions that reference a static variable type
	inline const http_type& GetHTTPType()
	{
		return http_type();
	}

	inline const int Code()
	{
		return http_type::Code;
	}

	inline const http_string_type CodeToString()
	{
		return http_type::CodeToString();
	}

	inline const http_string_type ResponseString()
	{
		return http_type::ResponseString;
	}

	// Generating the response buffer
	http_string_type GetResponseBuffer()
	{
		std::stringstream buffer;

		buffer << "HTTP/1.1 " << ResponseString() << "\r\n";

		for (auto& iter : HTTPBuffer < T >::_Buffer)
			buffer << iter.first << ": " << iter.second << "\r\n";

		buffer << "\r\n";

		if (_Content.size() > 0)
			buffer << _Content;

		_ResponseBuffer = buffer.str();

		return _ResponseBuffer;
	}

	int GetResponseLength()
	{
		return (int)_ResponseBuffer.length();
	}
};