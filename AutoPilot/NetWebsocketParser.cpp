#include "NetWebsocketParser.h"
#include "NetPlatformLinux.h"
#include "NetPacket.h"
#include "NetSessionManager.h"
#include "NetHTTPInterpreter.h"
#include "FlightController.h"
#include "AutoPilot.h"
#include "PID.h"
#include "rapidjson/document.h"
#include "rapidjson/writer.h"
#include "rapidjson/stringbuffer.h"
#include <exception>

NetWebsocketParser::NetWebsocketParser()
{

}

NetWebsocketParser::~NetWebsocketParser()
{

}

void NetWebsocketParser::ParseJson(std::string payload)
{
	rapidjson::Document document;

	if (document.Parse(payload.c_str()).HasParseError())
	{
		rapidjson::ParseErrorCode code = document.GetParseError();

		printf("Error %d parsing JSON string!\n", code);
		return;
	}

	if(document.HasMember("throttle"))
	{
		// Throttle received is in 0%~100% range but our throttle internally is 0~1000
		AutoPilot::Instance()->SetThrottle(document["throttle"].GetDouble() * 10.f);
	}

	if(document.HasMember("pidtuning"))
	{
		float p = 0, i = 0, d = 0;
		bool changed = false;

		rapidjson::Value& Tuning = document["pidtuning"];

		if(Tuning.HasMember("pterm")) {
			p = Tuning["pterm"].GetDouble();
			TuningPTerm.store(p);
			changed = true;
		}

		if(Tuning.HasMember("iterm")) {
			i = Tuning["iterm"].GetDouble();
			TuningITerm.store(i);
			changed = true;
		}

		if(Tuning.HasMember("dterm")) {
			d = Tuning["dterm"].GetDouble();
			TuningDTerm.store(d);
			changed = true;
		}

		if(changed == true)
		{
			AutoPilot::Instance()->GetFlightController()->SignalTuningParametersChanged();
		}
	}
}

void NetWebsocketParser::SendTelemetry(Vector Orientation, float Temperature, float Altitude)
{
	rapidjson::StringBuffer s;
	rapidjson::Writer<rapidjson::StringBuffer> writer(s);

	try
	{
		writer.StartObject();
		writer.String("sensors");
		writer.StartObject();

		writer.String("roll");
		writer.Double(Orientation.x);
		writer.String("pitch");
		writer.Double(Orientation.y);
		writer.String("yaw");
		writer.Double(Orientation.z);

		writer.String("temp");
		writer.Uint(Temperature);

		writer.String("altitude");
		writer.Uint(Altitude);

		writer.EndObject();
		writer.EndObject();

		SendJson(s.GetString());
	}
	catch(std::exception& e)
	{
		std::terminate();
	}
}

void NetWebsocketParser::SendMotorThrottles(float MotorN, float MotorE, float MotorS, float MotorW)
{
	rapidjson::StringBuffer s;
	rapidjson::Writer<rapidjson::StringBuffer> writer(s);

	try
	{
		writer.StartObject();
		writer.String("throttles");
		writer.StartObject();

		writer.String("motorN");
		writer.Double(MotorN);
		writer.String("motorE");
		writer.Double(MotorE);
		writer.String("motorS");
		writer.Double(MotorS);
		writer.String("motorW");
		writer.Double(MotorW);

		writer.EndObject();
		writer.EndObject();

		SendJson(s.GetString());
	}
	catch(std::exception& e)
	{
		std::terminate();
	}
}

void NetWebsocketParser::SendPID(float Yaw, float Pitch, float Roll)
{
	rapidjson::StringBuffer s;
	rapidjson::Writer<rapidjson::StringBuffer> writer(s);

	try
	{
		writer.StartObject();
		writer.String("pid");
		writer.StartObject();

		writer.String("roll");
		writer.Double(Roll);
		writer.String("pitch");
		writer.Double(Pitch);
		writer.String("yaw");
		writer.Double(Yaw);

		writer.EndObject();
		writer.EndObject();

		SendJson(s.GetString());
	}
	catch(std::exception& e)
	{
		std::terminate();
	}
}

void NetWebsocketParser::SendJson(std::string json)
{
	NetSessionManager::SessionMap Sessions = NetSessionManager::Instance()->getAllSessions();

	NetPacket *packet = new NetPacket();

	packet->PacketData.write(reinterpret_cast<const unsigned char*>(json.c_str()), json.length());
	packet->setPacketSize(json.length());

	NetHTTPInterpreter::EncodeAsFrame(packet, TextFrame);

	if(!Sessions.empty())
	{
		NetPlatformLinux::Instance()->sendtoSocket(Sessions.begin()->second, packet->getPacketBuffer(), packet->getPacketSize());
	}

	delete packet;
}

void NetWebsocketParser::OnConnectionSuccess()
{
	rapidjson::StringBuffer s;
	rapidjson::Writer<rapidjson::StringBuffer> writer(s);

	const PID& pid = AutoPilot::Instance()->GetFlightController()->GetFlightPolicy().GetYawPID();

	try
	{
		writer.StartObject();
		writer.String("pidtuning");
		writer.StartObject();

		writer.String("pterm");
		writer.Double(pid.GetTunePTerm());
		writer.String("iterm");
		writer.Double(pid.GetTuneITerm());
		writer.String("dterm");
		writer.Double(pid.GetTuneDTerm());

		writer.EndObject();
		writer.EndObject();

		SendJson(s.GetString());

		std::cout << "Sent tuning values " << std::endl;
	}
	catch(std::exception& e)
	{
		std::terminate();
	}
}