#pragma once

/** This implementation of WebSockets follows RFC6455 (http://tools.ietf.org/html/rfc6455) **/

#include "NetPacket.h"
#include "NetSession.h"
#include <map>

enum WebSocketOpcode
{
	TextFrame = 0x1,
	BinaryFrame,
	CloseFrame = 0x8,
	PingFrame,
	PongFrame
};

enum FrameErrorCode
{
	NormalClosure = 1000,
	GoingAway,
	ProtocolError,
	UnacceptableData,
	Reserved,
	Reserved2,
	Reserved3,
	InconsistentData,
	PolicyViolation,
	OversizedMessage,
	ExtensionNegotiationFail,
	UnexpectedCondition,
};

class NetHTTPInterpreter {
public:
	NetHTTPInterpreter(NetPacket *packet, NetSession *session);
	~NetHTTPInterpreter();

	NetHTTPInterpreter(const NetHTTPInterpreter&) = delete;
	NetHTTPInterpreter(NetHTTPInterpreter&&) = delete;
	NetHTTPInterpreter& operator=(NetHTTPInterpreter) = delete;
	NetHTTPInterpreter& operator=(NetHTTPInterpreter&&) = delete;

	void InterpretPacket();

	void AuthenticateHandshake();
	std::string CalcAcceptKey(std::string key);
	
	static bool DetectWebSocketUpgradeRequest(NetPacket *packet);
	static void SendWebSocketUpgradeRequest(NetSession *session);
	static void HandleSocketUpgradeResponse(NetPacket *packet, NetSession *session);
	static bool HandleGetRequest(NetPacket *packet, NetSession *session);
	static void SendFileToSession(std::string path, NetSession* session);
	static void EncodeAsFrame(NetPacket *packet, WebSocketOpcode opcode);
	static bool isHTTPPacket(NetPacket *packet);
	static std::string ErrorCodeToString(FrameErrorCode code);

private:
	NetPacket *httpPacket;
	NetSession *httpSession;
};

static const char *DocumentRoot = "web";