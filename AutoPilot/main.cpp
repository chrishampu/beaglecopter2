/*#include <thread>
#include <mutex>
#include <iostream>
#include <chrono>
#include <cstring>
#include <pthread.h>
 */
 
#include "AutoPilot.h"
#include "FlightController.h"
#include "IMU.h"
#include "MotorController.h"
#include "NetPlatformLinux.h"
#include <unistd.h>

int main()
{
  /*
    std::thread t1(f, 1);
 
    sched_param sch;
    int policy; 
 
    pthread_getschedparam(t2.native_handle(), &policy, &sch);
    sch.sched_priority = 19;
    if(pthread_setschedparam(t2.native_handle(), SCHED_FIFO, &sch)) {
        std::cout << "Failed to setschedparam: " << std::strerror(errno) << '\n';
    }

    t1.join();
  */

    NetPlatformLinux::Instance()->openConnectTo("192.168.0.16", 9999, WebSocket, true);

    IMU::Instance()->Calibrate();

    MotorController::Instance()->ArmMotors();

    AutoPilot::Instance()->ChangeFlightPolicy<StabilizedFlight>();

    while(1)
    {
        IMU::Instance()->Poll();
        AutoPilot::Instance()->Update();
        MotorController::Instance()->Update();
        NetPlatformLinux::Instance()->Process();
    }

    return 0;
}
